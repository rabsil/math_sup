\chapter{Vecteurs}\label{chap:vectors}

\begin{detail}
Certains concepts peuvent être complètement décrits par une quantité scalaire, comme la température. En effet, s'il fait $23$°C., la température est entièrement décrite par cette mesure : le nombre $23$. Néanmoins, parfois, une telle mesure ne suffit pas à décrire certaines grandeurs physiques. On peut par exemple affirmer qu'un objet pèse $2$kg, mais cela ne précise pas que l'objet est attiré vers le sol. Similairement, lorsque qu'on accélère en voiture, donner la valeur de l'accélération ne précise pas que l'on est écrasé dans son siège.

Pour cette raison, les mathématiciens du XIX\textsubscript{e} siècle, en particulier Giusto Bellavitis et William Rowan Hamilton, ont introduit le concept de \emph{vecteur}, afin de compléter la notion de scalaire et de décrire de manière plus précise les phénomènes physiques qu'ils observaient. Intuitivement, les vecteurs décrivent à la fois une quantité mais également une \emph{direction}, pour décrire «~dans quel sens~» la quantité est pertinente. Ainsi, on peut parfaitement décrire qu'un objet pèse $2$kg et qu'il est attiré vers le bas.

Ce chapitre décrit premièrement les concepts de base liés aux vecteurs en section~\ref{sec:vect-concepts}, et définit ensuite les opérations habituellement utilisées sur les vecteurs en section~\ref{sec:vect-ops}. Comme d'habitude, ce chapitre est clôturé par des exercices résolus, en section~\ref{sec:vect-exos-res}.
\end{detail}

\section{Concepts de base}\label{sec:vect-concepts}

\begin{defbox}{}{}
Un \emph{vecteur} $\vec{v}$ à $n$ composantes est un n-uple $(v_1, v_2, \dots, v_n) \in \IR^n$, avec $n \in \IN$ et $n\geqslant 2$.\indexn{v}{$\vec{v}$}{Vecteur}\index[terminologie]{Vecteur!définition}
\end{defbox}

Quand $n=2$, on représente habituellement un vecteur $v$ dans un plan cartésien comme un segment de droite muni d'une flèche. Dans la suite de ce document, toutes les définitions, propositions et exemples seront énoncés pour $n=2$, mais peuvent être facilement étendues pour $n$ arbitraire.

\begin{example}
Soit $v=(3,2)$. On peut représenter $\vec{v}$ comme à la figure~\ref{fig:vect-example}. Sur cette figure, on remarque que la première composante décrit un déplacement sur l'axe $ox$, et la deuxième un déplacement sur l'axe $oy$.

\begin{figure}[!htp]
\centering
\begin{tikzpicture}
\draw[->] (-0.5,0) -- (5.5,0) node[right] {$x$};
\draw[->] (0,-0.5) -- (0,4.5) node[above] {$y$};

\foreach \i in {1,...,4}
{
	\draw[dotted] (0,\i) -- (5.5,\i);
	\draw (0,\i) -- (-0.25,\i) node[left] {\small $\i$};
}
\foreach \i in {1,...,5}
{
	\draw[dotted] (\i,0) -- (\i,4.5);
	\draw (\i,0) -- (\i,-0.25) node[below] {\small $\i$};
}

\draw[_arc, very thick, red] (1,1) -- (4,3) node[above, midway, sloped] {$\vec{v}$};
\draw[<->] (1,0.75) -- (4,0.75) node[midway,below] {$3$};
\draw[<->] (4.25,1) -- (4.25,3) node[midway,right] {$2$};
\end{tikzpicture}
\caption{Illustration de $v=(3,2)$}
\label{fig:vect-example}
\end{figure}
\end{example}

Notons que, parfois, on note un vecteur en nommant explicitement ses deux extrémités. 
\begin{nott}
Soient $A=(a_x,a_y)$ et $B=(b_x,b_y)$, le vecteur 
\begin{equation*}
\overrightarrow{AB}=(b_x-a_x,b_y-a_y)
\end{equation*} 
est le vecteur joignant $A$ à $B$. 
\end{nott}

\begin{example}
Soient $A=(1,2)$ et $B=(3,5)$, on a $\overrightarrow{AB} = (3-1,5-2) = (2,3)$.
\end{example}

\begin{propbox}{}{}
Soient $\vec{u} = (u_1,u_2) \in \IR^2$ et $\vec{v}=(v_1,v_2) \in \IR^2$, on dit que $\vec{u}$ et $\vec{v}$ sont \emph{égaux}, noté $\vec{u} = \vec{v}$, si et seulement si $u_1 = v_1$ et $u_2 = v_2$.\index[terminologie]{Vecteur!égalité}
\end{propbox}

En particulier, on remarque que cette définition d'égalité de vecteurs fait abstraction du «~départ~» d'un vecteur. Par exemple, les vecteurs $\vec{u}$ et $\vec{v}$ de $\IR^2$ illustrés à la figure~\ref{fig:vect-eg} sont égaux. En physique, souvent, la définition de vecteur est étendue pour tenir compte du point d'application d'un vecteur. Ce n'est pas le cas en mathématiques.

\begin{figure}[!htp]
\centering
\begin{tikzpicture}
\draw[->] (-0.5,0) -- (7.5,0) node[right] {$x$};
\draw[->] (0,-0.5) -- (0,5.5) node[above] {$y$};

\foreach \i in {1,...,5}
{
	\draw[dotted] (0,\i) -- (7.5,\i);
	\draw (0,\i) -- (-0.25,\i) node[left] {\small $\i$};
}
\foreach \i in {1,...,7}
{
	\draw[dotted] (\i,0) -- (\i,5.5);
	\draw (\i,0) -- (\i,-0.25) node[below] {\small $\i$};
}

\draw[_arc, very thick, red] (1,2) -- (4,4) node[above, midway, sloped] {$\vec{v}$};
\draw[_arc, very thick, blue] (3,1) -- (6,3) node[above, midway, sloped] {$\vec{u}$};
\end{tikzpicture}
\caption{Deux vecteurs égaux}
\label{fig:vect-eg}
\end{figure}

Souvent, en plus des composantes d'un vecteur, on veut souvent exprimer sa «~magnitude~». Dans $\IR^2$, cela correspond à trouver la longueur du segment joignant les deux extrémités du vecteur considéré. Par application du théorème de Pythagore, on définit en conséquence la \emph{norme} d'un vecteur comme suit.

\begin{defbox}{}{}
Soit $\vec{v}=(v_1,v_2) \in \IR^2$, on définit la \emph{norme} de $\vec{v}$, notée $\norm{\vec{v}}$, comme\index[terminologie]{Vecteur!norme}\indexn{bar}{$\lVert \vec{v} \rVert$}{Norme (vecteur)}
\begin{equation*}
\norm{\vec{v}} = \sqrt{v_1^2 + v_2^2}.\indexn{bb}{$\norm{\vec{v}}$}{Norme}
\end{equation*}
\end{defbox}

%En particulier, si $\vec{v} \in \IR^2$, on a $\norm{v} = \sqrt{v_1^2 + v_2^2}$. 

\begin{example}
Sur la figure~\ref{fig:vect-example}, on a $\vec{v} = (2,3)$ et, en conséquence, $\norm{\vec{v}} = \sqrt{2^2 + 3^2} = \sqrt{4 + 9} = \sqrt{13}$.
\end{example}

\begin{nott}
Le \emph{vecteur nul}, noté $\vec{0}$, est le vecteur dont toutes les composantes sont nulles. Sa norme est également nulle.\indexn{0}{$\vec{0}$}{Vecteur nul}
\end{nott}

\section{Opérations sur les vecteurs}\label{sec:vect-ops}

Diverses opérations sont définies sur les vecteurs. En particulier, il est possible de les additionner et soustraire. Ainsi, on peut par exemple dire que si des bûcherons écossais s'entraînent au tir de corde et que l'une équipe tire avec une force de $4\,000$N et l'autre avec une force de $5\,000$N, la tension totale dans la corde est de $9\,000$N, et la résultante de la force est de $1\,000$N en faveur de la deuxième équipe.

\subsection{Multiplication par un scalaire}

Une opération couramment utilisée dans le cas de calculs vectoriels est la multiplication par un scalaire. Intuitivement, cette opération permet d'augmenter la norme d'un vecteur (sa «~grandeur~»), sans en modifier la direction\footnote{Par direction, on entend ici le coefficient angulaire de la droite support d'un vecteur, pas le sens de la flèche de la représentation dans le plan.}.

\begin{defbox}{}{}
Soit $v = (v_1,v_2) \in \IR^2$ et $\lambda \in \IR$, on a
\begin{equation*}
\lambda \vec{v} = (\lambda v_1, \lambda v_2).
\end{equation*}
\end{defbox}

En d'autres termes, multiplier un vecteur $\vec{v}$ par un scalaire $\lambda$ consiste à multiplier chaque composante de $\vec{v}$ par $\lambda$.

\begin{example}
Soit $\vec{v} = (2, 1)$, on a $3\vec{v} = (2 \cdot 3, 1 \cdot 3) = (6,3)$. Cette situation est illustrée à la figure~\ref{fig:scalar-v}. Similairement, $-\vec{v} = (-2,-1)$.
\end{example}

\begin{figure}[!htp]
\centering
\begin{tikzpicture}
\draw[->] (-0.5,0) -- (7.5,0) node[right] {$x$};
\draw[->] (0,-0.5) -- (0,5.5) node[above] {$y$};
	
\foreach \i in {1,...,5}
{
	\draw[dotted] (0,\i) -- (7.5,\i);
	\draw (0,\i) -- (-0.25,\i) node[left] {\small $\i$};
}
\foreach \i in {1,...,7}
{
	\draw[dotted] (\i,0) -- (\i,5.5);
	\draw (\i,0) -- (\i,-0.25) node[below] {\small $\i$};
}
	
\draw[_arc, very thick, red] (3,2) -- (5,3) node[midway, above, sloped] {$\vec{v}$};
\draw[_arc, very thick, blue] (3,2) -- (1,1) node[midway, above, sloped] {$-\vec{v}$};

\draw[-|, thick, Green] (1,2) -- (3,3);
\draw[-|, thick, Green] (3,3) -- (5,4);
\draw[_arc, very thick, Green] (1,2) -- (7,5) node[midway, above, sloped] {$3\vec{v}$};
\end{tikzpicture}
\caption{Multiplication par un scalaire}
\label{fig:scalar-v}
\end{figure}

Par ailleurs, la multiplication scalaire permet de définir la notion de vecteurs «~parallèles~». En effet, si deux vecteurs sont parallèles, l'un de ces vecteurs est multiple de l'autre. On dit que de tels vecteurs sont \emph{colinéaires}.

\begin{defbox}{}{colin}
Soient $\vec{u}, \vec{v} \in \IR^2$, on dit que $\vec{u}$ et $\vec{v}$ sont \emph{colinéaires} si $\vec{u} = \lambda \vec{v}$ pour un certain $\lambda \in \IR_0$.\indext{Colinéaire}
\end{defbox}

Les vecteurs colinéaires ont une certaine importance dans le cadre de résolution de systèmes. En effet, la notion de linéarité généralise la notion de parallélisme pour des droites. Dans la mesure où des systèmes incluant des droites parallèles ont rarement une solution, ces configurations sont généralement écartées.

\subsection{Addition et soustraction}

Une autre opération couramment utilisée est l'addition et la soustraction de vecteurs. Ces opérations ont de nombreuses applications, comme en physique où elles permettent, par exemple, de calculer la résultante de forces.

\begin{defbox}{}{}
Soient $\vec{u} = (u_1,u_2) \in \IR^2$ et $\vec{v}=(v_1,v_2) \in \IR^2$, on définit\index[terminologie]{Vecteur!somme}\index[terminologie]{Vecteur!différence}
\begin{align*}
\vec{u} + \vec{v} &= (u_1 + v_1, u_2 + v_2),\\
\vec{u} - \vec{v} &= (u_1 - v_1, u_2 - v_2).
\end{align*}
\end{defbox}

Intuitivement, l'addition et la soustraction de vecteurs est effectuée composante par composante. En conséquence, on remarque que la somme de vecteurs est commutative, mais pas la différence.

\begin{example}
Soient $\vec{u} = (1,2)$ et $\vec{v} = (-1,3)$. On a 
\begin{itemize}
\item $\vec{u} + \vec{v} = (1 - 1, 2 + 3) = (0, 5)$,
\item $\vec{u} - \vec{v} = (1 + 1, 2 - 3) = (2, -1)$.
\end{itemize}
\end{example}

\subsubsection*{Relation de Chasles}

Dans le cas des vecteurs de $\IR^2$, on peut construire géométriquement la somme ou de la différence de deux vecteurs en utilisant la \emph{relation de Chasles}, aussi appelée «~règle du parallélogramme~».\indext{Relation de Chasles}\indext{Règle du parallélogramme}

L'utilisation de cette règle est illustrée à la figure~\ref{fig:chasles}. Sur cette figure, on remarque que l'on peut reporter le vecteur $\vec{u}$ à l'extrémité du vecteur $\vec{v}$, et inversement, afin de former un parallélogramme. L'addition dénote l'une des diagonales, et la différence l'autre diagonale.

\begin{figure}[!htp]
\centering
\subfigure[$\vec{u} + \vec{v}$]
{
	\begin{tikzpicture}[scale=.6]
	\draw[->] (-0.5,0) -- (8.5,0) node[right] {$x$};
	\draw[->] (0,-0.5) -- (0,5.5) node[above] {$y$};
	
	\foreach \i in {1,...,5}
	{
		\draw[dotted] (0,\i) -- (8.5,\i);
		\draw (0,\i) -- (-0.25,\i) node[left] {\small $\i$};
	}
	\foreach \i in {1,...,8}
	{
		\draw[dotted] (\i,0) -- (\i,5.5);
		\draw (\i,0) -- (\i,-0.25) node[below] {\small $\i$};
	}
	
	\draw[black,domain=3.75:8.25,smooth, variable=\x] plot ({\x},{-0.25*\x+5});	
	\draw[black,domain=4.75:8.25,smooth, variable=\x] plot ({\x},{0.66*\x-2.3});
	
	\draw[_arc, very thick, red] (1,2) -- (4, 4) node[above, midway, sloped] {$\vec{v}$};
	\draw[_arc, very thick, red!50] (5,1) -- (8, 3) node[above, midway, sloped] {$\vec{v}$};
	\draw[_arc, very thick, blue] (1,2) -- (5, 1) node[above, midway, sloped] {$\vec{u}$};
	\draw[_arc, very thick, Green] (1,2) -- (8,3) node[above, midway, sloped] {$\vec{u} + \vec{v}$};			
	\end{tikzpicture}
}
\subfigure[$\vec{u} - \vec{v}$]
{
	\begin{tikzpicture}[scale=.6]
	\draw[->] (-0.5,0) -- (8.5,0) node[right] {$x$};
	\draw[->] (0,-0.5) -- (0,5.5) node[above] {$y$};
	
	\foreach \i in {1,...,5}
	{
		\draw[dotted] (0,\i) -- (8.5,\i);
		\draw (0,\i) -- (-0.25,\i) node[left] {\small $\i$};
	}
	\foreach \i in {1,...,8}
	{
		\draw[dotted] (\i,0) -- (\i,5.5);
		\draw (\i,0) -- (\i,-0.25) node[below] {\small $\i$};
	}
	
	\draw[black,domain=3.75:8.25,smooth, variable=\x] plot ({\x},{-0.25*\x+5});	
	\draw[black,domain=4.75:8.25,smooth, variable=\x] plot ({\x},{0.66*\x-2.3});
	
	\draw[_arc, very thick, red] (1,2) -- (4, 4) node[above, midway, sloped] {$\vec{v}$};
	\draw[_arc, very thick, blue] (1,2) -- (5, 1) node[above, midway, sloped] {$\vec{u}$};
	\draw[_arc, very thick, blue!50] (4,4) -- (8, 3) node[above, midway, sloped] {$\vec{u}$};
	\draw[_arc, very thick, red!50] (8,3) -- (5, 1) node[above, midway, sloped] {$-\vec{v}$};
	\draw[_arc, very thick, Green] (4,4) -- (5,1) node[above, midway, sloped] {$\vec{u} - \vec{v}$};	
	\end{tikzpicture}
}
\caption{Addition et soustraction de vecteurs}
\label{fig:chasles}
\end{figure}

Par ailleurs, on note que la multiplication par un scalaire distribue la somme et différence de vecteurs. Ceci est exprimé par la propriété suivante.

\begin{propbox}{}{}
Soient $\vec{u}, \vec{v}$ et $\lambda \in \IR$, on a
\begin{align*}
\lambda (\vec{u} + \vec{v}) &= \lambda \vec{u} + \lambda \vec{v}\\
\lambda (\vec{u} - \vec{v}) &= \lambda \vec{u} - \lambda \vec{v}
\end{align*}
\end{propbox}

De plus, on remarque que souvent, en mathématiques, on définit un vecteur comme le résultat de l'addition et multiplication scalaire d'autres vecteurs, par exemple, on peut écrire $\vec{u} = 3\vec{v} - 4 \vec{w}$. Une telle écriture est appelée une \emph{combinaison linéaire}.

\begin{example}
Soient $u = (1,2)$, $v_1 = (1,0)$, $v_2 = (-1,1)$ et $v_3 = (2,-1)$. On a 
\begin{equation*}
u = 1 v_1 + 2 v_2 + 0 v_3.
\end{equation*}
\end{example}

\begin{defbox}{}{}
Soient $\vec{v_1}, \vec{v_2}, \dots \vec{v_p} \in \IR^n$, tout vecteur $\vec{u}$ de la forme
\begin{equation*}
\vec{u} = \lambda_1 \vec{v_1} + \lambda_2 \vec{v_2} + \dots + \lambda_p \vec{v_p}
\end{equation*}
est une \emph{combinaison linéaire} de $\vec{v_1}, \dots, \vec{v_p}$, avec $\lambda_1, \dots, \lambda_p \in \IR$ non tous nuls.\indext{Combinaison linéaire}
\end{defbox}

On dit aussi que $\vec{u}$ \emph{dépend linéairement} de $\vec{v_1}, \dots, \vec{v_p}$. De plus amples informations au sujet de la dépendance et de l'indépendance linéaire seront fournis en section~\ref{sec:indep-lin} du chapitre~\ref{chap:matrix}.

\subsection{Produit scalaire}

Finalement, un dernier opérateur de calcul vectoriel est le \emph{produit scalaire}. Cet opérateur a de nombreuses applications en physique et en infographie, entre autres. Contrairement au cas de l'addition, soustration et multiplication par un scalaire, le résultat d'un produit scalaire \emph{n'} est \emph{pas} un vecteur, mais un nombre réel. %TODO : add reference for applications

Formellement, on définit le produit scalaire comme suit.

\begin{defbox}{}{}
Soient $\vec{u}=(u_1,u_2) \in \IR^2$, $\vec{v}=(v_1,v_2) \in \IR^2$ et $\theta$ l'angle formé entre $\vec{u}$ et $\vec{v}$, le \emph{produit scalaire} entre $\vec{u}$ et $\vec{v}$, noté $\vec{u} \cdot \vec{v}$, est défini comme\indexnt{uv}{$\vec{u} \cdot \vec{v}$}{Produit scalaire}
\begin{align*}
\vec{u} \cdot \vec{v} &= \norm{u} \cdot \norm{v} \cdot \cos(\theta)\\
					  &= u_1v_1 + u_2v_2\\
\end{align*}
\end{defbox}

Notez ici que l'opérateur «~$\cdot$~» entre $\vec{u}$ et $\vec{v}$, à gauche, n'est pas le même opérateur que le «~$\cdot$~» entre $\norm{u}$, $\norm{v}$ et $\cos(\theta)$, à droite. Le premier est un opérateur entre vecteurs, le second entre réels.

Géométriquement, on peut construire le produit scalaire par un système de projection, tel qu'illustré à la figure~\ref{fig:prod-proj}.

\begin{figure}[!htp]
\begin{center}
\subfigure[Angle entre $0$ et $\frac{\pi}{2}$]
{
	\begin{tikzpicture}
	\node[left] at (0,0) {$A$};
	\draw[->,very thick, red] (0,0) -- (2,2) node[above right] {\color{black} $C$};
	\coordinate (c) at (2,2);
	\coordinate (a) at (0,0);
	\draw[->,very thick, blue] (0,0) -- (3,0) node[right] (b) {\color{black} $B$};
	\draw[dashed] (2,2) -- (2,0) node[below] {$H$};
	\draw (2.15,0.35) -- (2.15, 0.15) -- (2.35,0.15);	
	\pic["\textcolor{Green}{$\theta$}", draw=Green, angle eccentricity=1.2, angle radius=1cm] {angle= b--a--c};
	
	\node at (1.5,-1) {$\overrightarrow{AB} \cdot \overrightarrow{BC} = |AB| \cdot |AH|$};
	\end{tikzpicture}
}\hspace*{1cm}
\subfigure[Angle entre $\frac{\pi}{2}$ et $\pi$]
{
	\begin{tikzpicture}
	\node[below] at (0,0) {$A$};
	\draw[->,very thick, red] (0,0) -- (-1,2) node[above] {\color{black} $C$};
	\draw[->,very thick, blue] (0,0) -- (3,0) node[right] (b) {\color{black} $B$};
	\coordinate (c) at (-1,2);
	\coordinate (a) at (0,0);
	\draw[dashed] (-1,2) -- (-1,0) node[below] {$H$};
	\draw (0,0) -- (-1.5,0);
	\draw (-1.15,0.35) -- (-1.15, 0.15) -- (-1.35,0.15);
	\pic["\textcolor{Green}{$\theta$}", draw=Green, angle eccentricity=1.2, angle radius=1cm] {angle= b--a--c};
	
	\node at (1.5,-1) {$\overrightarrow{AB} \cdot \overrightarrow{BC} = -|AB| \cdot |AH|$};
	\end{tikzpicture}
}
\caption{Construction géométrique du produit scalaire}
\label{fig:prod-proj}
\end{center}
\end{figure}

\begin{example}
Soient $\vec{u} = (0,3)$ et $\vec{v} = (4,4)$, illustrés la figure~\ref{fig:scalar-prod}. On remarque avant tout que l'angle entre $\vec{u}$ et $\vec{v}$ est de $45$° = $\frac{\pi}{4}$. On a donc
\begin{align*}
\vec{u} \cdot \vec{v} &= \norm{u} \cdot \norm{v} \cdot \cos\left(\frac{\pi}{4}\right)\\ %mistake here
					  &= \sqrt{0^2 + 3^2} \cdot \sqrt{4^2 + 4^2} \cdot \frac{\sqrt{2}}{2}\\
					  &= 3 \cdot \sqrt{32} \cdot \frac{\sqrt{2}}{2}\\
					  &= 3 \cdot 4 \sqrt{2} \cdot \frac{\sqrt{2}}{2}\\
					  &= 12.
\end{align*}
Similairement, on peut calculer ce produit scalaire comme
\begin{align*}
\vec{u} \cdot \vec{v} &= 0 \cdot 4 + 3 \cdot 4\\
					  &= 12\\
\end{align*}


Cette situation est illustrée à la figure~\ref{fig:scalar-prod}.

\begin{figure}[!htp]
\centering
\begin{tikzpicture}
\draw[->] (-0.5,0) -- (7.5,0) node[right] {$x$};
\draw[->] (0,-0.5) -- (0,5.5) node[above] {$y$};
	
\foreach \i in {1,...,5}
{
	\draw[dotted] (0,\i) -- (7.5,\i);
	\draw (0,\i) -- (-0.25,\i) node[left] {\small $\i$};
}
\foreach \i in {1,...,7}
{
	\draw[dotted] (\i,0) -- (\i,5.5);
	\draw (\i,0) -- (\i,-0.25) node[below] {\small $\i$};
}

\coordinate (p1) at (1,4);
\coordinate (p2) at (1,1);
\coordinate (p3) at (5,5);
\draw[_arc,red, very thick] (p2) -- (p1) node[midway,below left] {$\vec{u}$};
\draw[_arc,blue, very thick] (p2) -- (p3) node[midway,below,sloped] {$\vec{v}$};
\pic["\textcolor{Green}{$\theta$}", draw=Green, angle eccentricity=1.2, angle radius=1cm] {angle= p3--p2--p1};

\draw[black,domain=0.75:2.5,smooth, variable=\x] plot ({\x},{-\x+5});
\draw (2.3,2.9) -- (2.5,2.7) -- (2.7,2.9);
\draw[<->] (1.2,0.8) -- (2.7,2.3) node[midway, below, sloped] {\footnotesize $\norm{u} \cdot \cos(\theta)$};
\end{tikzpicture}
\caption{Produit scalaire de deux vecteurs}
\label{fig:scalar-prod}
\end{figure}
\end{example}

Par ailleurs, on remarque le produit scalaire distribue l'addition et la différence de vecteurs. Ceci est exprimé par la propriété suivante.
\begin{propbox}{}{}
Soient $\vec{u}_1, \vec{u}_2, \vec{v}_1, \vec{v}_2$, on a
\begin{equation*}
(\vec{u}_1 + \vec{v}_1) \cdot (\vec{u_2} + \vec{v_2}) = \vec{u}_1 \cdot \vec{u}_2 + \vec{u}_1 \cdot \vec{v}_2 + \vec{v}_1 \cdot \vec{u}_2 + \vec{v}_1 \cdot \vec{v}_2
\end{equation*}
\end{propbox}

De plus, on peut dégager une propriété importante de vecteurs \emph{orthogonaux}, c'est-à-dire de vecteurs dont les droites support sont perpendiculaires.

\begin{propbox}{}{}
Soient $\vec{u}, \vec{v} \in \IR^2$ orthogonaux, on a $\vec{u} \cdot \vec{v} = 0$.
\end{propbox}

En effet, par définition, si $\vec{u}$ et $\vec{v}$ sont orthogonaux, l'angle formé par ces deux vecteurs est égal à $\frac{\pi}{2}$ ou $\frac{3\pi}{2}$. Comme $\cos\left(\frac{\pi}{2}\right)= \cos\left(\frac{3\pi}{2}\right) = 0$, on a $\vec{u} \cdot \vec{v} = 0$.

\begin{detail}
\section{Exercices résolus}\label{sec:vect-exos-res}

\begin{exo}
Soient les vecteurs $\vec{t}$, $\vec{u}$, $\vec{v}$ et $\vec{w}$ illustrés à la figure~\ref{fig:exo-vec1}. Calculez
\begin{enumerate}
\item les composantes de $\vec{t}$, $\vec{u}$, $\vec{v}$ et $\vec{w}$,
\item $\norm{\vec{t}}$, $\norm{\vec{u}}$, $\norm{\vec{v}}$ et $\norm{\vec{w}}$,
\item $\vec{u} \cdot \vec{v}$, $\vec{t} \cdot \vec{w}$, $\vec{t} \cdot \vec{u}$ et $\vec{v} \cdot \vec{w}$.
\end{enumerate}

\begin{figure}[!htp]
\centering
\begin{tikzpicture}
\draw[->] (-0.5,0) -- (5.5,0) node[right] {$x$};
\draw[->] (0,-0.5) -- (0,4.5) node[above] {$y$};

\foreach \i in {1,...,4}
{
	\draw[dotted] (0,\i) -- (5.5,\i);
	\draw (0,\i) -- (-0.25,\i) node[left] {\small $\i$};
}
\foreach \i in {1,...,5}
{
	\draw[dotted] (\i,0) -- (\i,4.5);
	\draw (\i,0) -- (\i,-0.25) node[below] {\small $\i$};
}

\draw[_arc, very thick, Dandelion] (5,4) -- (2,4) node[above, midway, sloped] {$\vec{t}$};
\draw[_arc, very thick, red] (1,1) -- (4,4) node[above, midway, sloped] {$\vec{u}$};
\draw[_arc, very thick, blue] (0,2) -- (4,0) node[above, midway, sloped] {$\vec{v}$};
\draw[_arc, very thick, Green] (4,1) -- (1,3) node[above, midway, sloped] {$\vec{w}$};
\end{tikzpicture}
\caption{Quatre vecteurs}
\label{fig:exo-vec1}
\end{figure}
\end{exo}

\begin{sol}
On procède comme suit.
\begin{enumerate}
\item On a 
	\begin{itemize}
	\item $\vec{t} = (2 - 5, 4 - 4) = (-3,0)$,
	\item $\vec{u} = (4 - 1, 4 - 1) = (3,3)$,
	\item $\vec{v} = (4 - 0, 0 - 2) = (4,-2)$,
	\item $\vec{w} = (1 - 4, 1 - 3) = (-3,2)$.
	\end{itemize}
\item Les normes sont calculées comme
	\begin{itemize}
	\item $\norm{t} = \norm{(-3,0)} = \sqrt{(-3)^2+0^2} = 3$,
	\item $\norm{u} = \norm{(3,3)} = \sqrt{3^2+3^2} = \sqrt{18} = 3\sqrt{2}$,
	\item $\norm{v} = \norm{(4,-2)} = \sqrt{4^2+(-2)^2} = \sqrt{20} = 2\sqrt{5}$,
	\item $\norm{w} = \norm{(-3,2)} = \sqrt{(-3)^2+(-2)^2} = \sqrt{13}$.
	\end{itemize}
\item On obtient
	\begin{itemize}
	\item $\vec{u} \cdot \vec{v} = 3 \cdot 4 + 3 \cdot (-2) = 6$,
	\item $\vec{t} \cdot \vec{w} = (-3) \cdot (-3) + 0 \cdot (-2) = 9$,
	\item $\vec{t} \cdot \vec{u} = (-3) \cdot 3 + 0 \cdot 3 = -9$,
	\item $\vec{v} \cdot \vec{w} = 4 \cdot (-3) + (-2) \cdot (-2) = -8$.
	\end{itemize}
\end{enumerate}
\end{sol}

\begin{exo}
Calculez les composantes des vecteurs suivants :
\begin{enumerate}[a)]
\item $(1,2) + (-4,3)$,
\item $2 (-1,4) - 3 (2,5)$,
\item $-(1,2) + 5 (3,-2)$,
\item $(5,-3) - (2,-1)$.
\end{enumerate}
\end{exo}

\begin{sol}
On a 
\begin{enumerate}[a)]
\item $(1,2) + (-4,3) = (1 - 4, 2+3) = (-3,5)$,
\item $2 (-1,4) - 3 (2,5) = (-2,8) - (6,15) = (-8,-7)$,
\item $-(1,2) + 5 (3,-2) = (-1+15,-2-10) = (14,-12)$,
\item $(5,-3) - (2,-1) = (3,2)$.
\end{enumerate}
\end{sol}

\begin{exo}
Illustrez les vecteurs suivants sur l'hexagone de la figure~\ref{fig:exo-vec2} :
\begin{enumerate}[a)]
\item $\overrightarrow{AB} + \overrightarrow{OE}$,
\item $2\overrightarrow{FO} + \overrightarrow{BO} + \overleftarrow{CB}$,
\item $\overrightarrow{BO} + \overrightarrow{CD}$,
\item $2\overrightarrow{EF} - \overrightarrow{DF} + \overrightarrow{DC}$.
\end{enumerate}
\end{exo}

\begin{figure}[!htp]
\begin{center}
\begin{tikzpicture}[scale=.75]
\foreach \i/\j in {0/A, 1/B, 2/C, 3/D, 4/E, 5/F}
{
	\pgfmathtruncatemacro{\angle}{\i*60}
	\pgfmathtruncatemacro{\next}{(\i+1)*60}
	\node[_dot] at (\angle:3) {};
	\node at (\angle:3.5) {$\j$};
	\draw (0,0) -- (\angle:3) -- (\next:3);	
}

\node[_dot] at (0,0) {};
\node at (0,-0.5) {$O$};
\end{tikzpicture}
\caption{Un hexagone}
\label{fig:exo-vec2}
\end{center}
\end{figure}

\begin{sol}
On a 
\begin{enumerate}[a)]
\item $\overrightarrow{OE} = \overrightarrow{BO}$, et donc $\overrightarrow{AB} + \overrightarrow{OE} = \overrightarrow{AB} + \overrightarrow{BO} = \overrightarrow{AO}$,
\item $2\overrightarrow{FO} = \overrightarrow{FC}$, $\overrightarrow{BO} = \overrightarrow{CD}$ et $\overrightarrow{CB} = \overrightarrow{DO}$, donc $2\overrightarrow{FO} + \overrightarrow{BO} + \overrightarrow{CB} = \overrightarrow{FC} + \overrightarrow{CD} + \overrightarrow{DO} = \overrightarrow{FD}$,
\item $-\overrightarrow{CD} = \overrightarrow{DC} = \overrightarrow{OB}$, en conséquence $\overrightarrow{BO} + \overrightarrow{CD} =\overrightarrow{BO} + \overrightarrow{OB} = \vec{0}$,
\item $-\overrightarrow{DF} = \overrightarrow{FD}$, donc 
\begin{align*}
2\overrightarrow{EF} - \overrightarrow{DF} + \overrightarrow{DC} &= 2\overrightarrow{EF} + \overrightarrow{FD} + \overrightarrow{DC} \\
		&= 2\overrightarrow{EF} + \overrightarrow{FC}\\
		&= \overrightarrow{EF} + \overrightarrow{EF} + \overrightarrow{FC}\\
		&= \overrightarrow{EF} + \overrightarrow{EC},
\end{align*}
or $\overrightarrow{EC} = \overrightarrow{FB}$, donc $\overrightarrow{EF} + \overrightarrow{EC} = \overrightarrow{EF} + \overrightarrow{FB} = \overrightarrow{EB}$.
\end{enumerate}

\begin{figure}[!htp]
\begin{center}
\begin{tikzpicture}[scale=.75]
\foreach \i/\j in {0/A, 1/B, 2/C, 3/D, 4/E, 5/F}
{
	\pgfmathtruncatemacro{\angle}{\i*60}
	\pgfmathtruncatemacro{\next}{(\i+1)*60}
	\node[_dot] at (\angle:3) {};
	\node at (\angle:3.5) {$\j$};
	\draw (0,0) -- (\angle:3) -- (\next:3);	
}

\node[_dot] at (0,0) {};
\node at (0,-0.5) {$O$};

\draw[_arc, very thick, red] (-60:3) -- (120:3) node[above, midway,xshift=0.75cm,yshift=0.75cm] {\footnotesize $2\overrightarrow{EF} - \overrightarrow{DF} + \overrightarrow{DC}$};
\draw[_arc, very thick, blue] (-3,0) -- (0,0) node[above, midway] {\footnotesize $\overrightarrow{AB} + \overrightarrow{OE}$};
\draw[_arc, very thick, Green] (3,0) -- (0,0) node[below, xshift=0.5cm, midway] {\footnotesize $2\overrightarrow{FO} + \overrightarrow{BO} + \overrightarrow{CB}$};
\end{tikzpicture}
\caption{Illustration du calcul de vecteurs}
\label{fig:exo-vec2-sol}
\end{center}
\end{figure}
\end{sol}

\begin{exo}
Soient les points $A=(1,0)$, $B=(5,4)$, $C=(1,8)$, et $X$ le point du plan tel que
\begin{equation*}
2 \overrightarrow{XA} - 3 \overrightarrow{XB} + 2 \overrightarrow{XC} = \vec{0}.
\end{equation*}
Quelles sont les coordonnées de $X$ ?
\end{exo}

\begin{sol}
Posons $X=(p_x, p_y)$. On veut
\begin{align*}
	 & 2 \overrightarrow{XA} - 3 \overrightarrow{XB} + 2 \overrightarrow{XC} = \vec{0}\\
\ssi~& 2 (1 - p_x, 0 - p_y) - 3 (5 - p_x, 4 - p_y) + 2 (1 - p_x, 8 - p_y) = (0,0)\\
\ssi~& (2 - 2p_x, -2p_y) - (15-3p_x, 12-3p_y) + (2 - 2p_x, 16-2p_y) = (0,0)\\
\ssi~& (2 - 15 + 2 - 2p_x+3p_x-2p_x, -12 + 16 - 2p_y+3p_y-2p_y) = (0,0)\\
\ssi~& (-11-p_x,4-p_y) = (0,0)\\
\ssi~& \left\{
	\begin{array}{l}
	p_x=-11,\\
	p_y=4
	\end{array}
\right.
\end{align*}
On a donc $X=(-11,4)$.
\end{sol}

\begin{exo}
Soient les points $A=(1,1)$ et $B=(3,2)$. Quelle est la norme de $\vec{u}=3\overrightarrow{AB}$
\end{exo}

\begin{sol}
On a $\overrightarrow{AB} = (3 - 1, 2 - 1) = (2,1)$. Dès lors, $3\overrightarrow{AB} = (6,3)$. Dès lors,
\begin{align*}
\norm{3\overrightarrow{AB}} &= \sqrt{6^2+3^2}\\
							&= \sqrt{36 + 9}\\
							&= \sqrt{45}\\
							&= 3\sqrt{5}
\end{align*}
\end{sol}

\begin{exo}
Soient $\vec{u}$ et $\vec{v}$ tels que $\norm{u} = \norm{v} = 4$ et $\vec{u} \cdot \vec{v} = 3$. Que vaut $\norm{\vec{u} + 2\vec{v}}$ ?
\end{exo}

\begin{sol}
Avant toutes choses, notons $\vec{u} = (u_x, u_y)$ et $\vec{v} = (v_x, v_y)$. On sait que
\begin{align}
\norm{u} = 4 &\ssi \sqrt{u_x^2+u_y^2} = 4,\label{eq:ex-v1}\\
\norm{v} = 4 &\ssi \sqrt{v_x^2+v_y^2} = 4,\label{eq:ex-v2}\\
\vec{u} \cdot \vec{v} = 3 &\ssi u_xu_y + v_xv_y = 3.\label{eq:ex-v3}
\end{align}

On a 
\begin{align*}
\norm{\vec{u} + 2\vec{v}} &= \norm{(u_x+2v_x, u_y+2v_y)}\\
						  &= \sqrt{(u_x+2v_x)^2+(u_y+2v_y)^2}\\
						  &= \sqrt{(u_x^2 + 4v_x^2 + 4u_xv_x) + (u_y^2 + 4v_y^2 + 4u_yv_y) }\\
						  &= \sqrt{(u_x^2 + u_y^2) + 4 (v_x^2+ v_y^2) + 4 (u_xv_x + u_yv_y) }\\
						  &= \sqrt{16 + 4 \cdot 16 + 4 \cdot 3} \\
						  &~~~~~~~~~~~~~~~\textrm{ par les équations } \eqref{eq:ex-v1}, \eqref{eq:ex-v2} \textrm{ et } \eqref{eq:ex-v3}\\
						  &= \sqrt{92}\\
						  &= 2\sqrt{23}
\end{align*}
\end{sol}
\end{detail}
