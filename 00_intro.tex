\chapter*{Introduction}
\addcontentsline{toc}{chapter}{Introduction}

L'étude des mathématiques est une tâche compliquée, qui requiert de la rigueur, du temps, et de la pratique. Le cours de mathématiques à l'école supérieure d'informatique (ESI) s'adresse à des étudiants sortant tout juste du secondaire, et venant d'horizons divers, certains ayant eu jusqu'à huit heures de mathématiques par semaine, d'autres seulement deux. Le but de ce cours est de mettre tous les étudiants sur un même pied d'égalité afin qu'ils puissent poursuivre leurs études sans difficultés liées à l'enseignement suivi dans le secondaire.

Notez toutefois que le cours de mathématiques ne comporte actuellement que peu d'heures au programme, il est donc impossible de voir l'intégralité des mathématiques utilisées dans le cursus de bachelier dans ce seul syllabus. À ce propos, on considère qu'une bonne partie du programme de mathématiques donné à raison de 4h par semaine dans l'enseignement secondaire général est acquis\footnote{Des rappels, annexes à ce document, peuvent être consultés pour les concepts moins familiers à l'étudiant.}.

\section*{À propos de ce syllabus}
\addcontentsline{toc}{section}{À propos de ce syllabus}

Ce syllabus est un support pour le cours théorique dispensé en auditoire. L'étudiant y trouvera tous les concepts et informations nécessaires afin de réussir cette activité d'apprentissage. En plus de cela, un syllabus d'exercices non résolus est également disponible, exercices qui seront réalisés soit en cours, soit en séance de remédiation. 

Notez également que le syllabus de théorie ne doit pas être utilisé comme un «~roman~», à lire de la première à la dernière page. Sa taille est trop conséquente pour une telle tâche, surtout étant donné le temps imparti pour le cours de mathématiques. Il doit plutôt être utilisé comme support : une référence à consulter \emph{ponctuellement} quand le besoin s'en fait sentir par l'étudiant. Ce besoin de provenir de plusieurs origines : des notes de cours peu claires ou incomplètes, un dessin explicatif, la nécessité de précision sur une définition, l'envie de consulter des exercices avec solutions, l'envie de plus d'exercices que ceux qui ont été dispensés. 

Ainsi, on peut partitionner le syllabus en deux types de contenu.
\begin{itemize}
%\item Les exercices non résolus : c'est sur des exercices similaires que l'étudiant sera interrogé à l'examen. Les réussir seul est donc un bon indice de sa maîtrise de la matière. De plus, les exercices primordiaux à résoudre sont flanqués du symbole \LeftThumbsUp, facilitant leur identification au sein du document.
\item La théorie : les définitions, propriétés, théorèmes, etc., à connaître afin de pouvoir résoudre les exercices. Ces éléments sont mis en évidence par un environnement en boite bleue dotée d'une numérotation commune (définition 1.1, théorème 1.2, etc.). Ces concepts sont mis en évidence par un encadré bleu.
\item Tout le reste : les nombreux exemples et figures, les exercices résolus, les intuitions, etc., sont tous présents pour aider à comprendre et assimiler la théorie afin que l'étudiant puisse résoudre les exercices sans encombres.\\
\end{itemize}

À cet effet, notez que ce document dispose d'une table des matières ainsi que d'un index des notations, définitions et de la terminologie afin de faciliter votre recherche. Dès lors, étant donné cette volonté délibérée d'usage ponctuel, l'étudiant ne doit pas être effrayé par l'épaisseur de cet ouvrage.

\section*{Réussir le cours de mathématiques}
\addcontentsline{toc}{section}{Réussir le cours de mathématiques}

Malgré sa taille imposante, le syllabus n'est qu'un support, c'est-à-dire un ensemble de pages susceptibles d'aider les étudiants dans leur cursus. Dans la plupart des cas, se contenter de lire ce document ne suffit \emph{pas} pour réussir l'examen. Comme mentionné en début de chapitre, faire des mathématiques requiert de la rigueur, du temps et de la pratique. Ainsi, ce document est fourni pour permettre à l'étudiant de développer sa capacité d'\emph{autonomie} et de \emph{réflexion personnelle} dans la pratique de notions mathématiques de base.

Pour ces raisons, il est vivement recommandé d'assister à chacun des cours dispensés, les étudiants y trouveront souvent des informations complémentaires liées aux concepts enseignés, qui peuvent faciliter leur compréhension. De plus, certains professeurs prévoient du temps pour permettre aux étudiants de travailler sur leurs exercices en classe, et fournissent une correction individuelle pour ces exercices. De telles séances permettent à l'étudiant de s’entraîner, et de se familiariser à la rigueur qui est demandée d'avoir pour l'examen.

Néanmoins, encore une fois, aller au cours et se servir du syllabus judicieusement ne suffit généralement pas non plus pour réussir l'examen. Il sera souvent nécessaire à l'étudiant de s’entraîner lui-même chez lui, en refaisant à la main les exercices vus au cours, sans la solution, ou en prenant l'initiative de résoudre les exercices non résolus au cours. Les professeurs dispensant le cours peuvent corriger de tels exercices, une fois une solution rédigée par l'étudiant. 

Une telle pratique offre de l'expérience dans l'utilisation des mathématiques, cette expérience est fondamentale à la maîtrise des concepts. Pour cette raison, se contenter de lire les solutions des exercices résolus est souvent inutile : même si l'étudiant comprend ce qui y est fait, il n’acquiert pas l'expérience qu'il aurait pu acquérir en résolvant l'exercice lui-même.

En conclusion, même si ce syllabus est un support complet, c'est à l'étudiant de former lui-même son expérience dans l'utilisation des mathématiques, via les cours magistraux et le travail fourni à la maison.