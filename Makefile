BUILD=build
LATEX=latexmk -recorder -pdf -pdflatex="pdflatex --shell-escape %O %S" \
		-outdir=$(BUILD)
MAIN=syllabus

detail: $(MAIN)-detail.pdf

short: $(MAIN).pdf

exos: $(MAIN)-exos.pdf

all: detail short exos


preview:
	$(LATEX) -pvc -interaction=nonstopmode $(MAIN).tex

%.pdf: latexmkbuild
	$(LATEX) $(patsubst %.pdf, %.tex, $@)
	ln -fs $(BUILD)/$@

clean:
	rm -rf $(BUILD) _minted-*
	rm -f $(MAIN)*.pdf

# This recipe is always made (because it is phony). Because we need to rely on
# latexmk to check for file dependencies.
latexmkbuild:
	mkdir -p $(BUILD)

.PHONY: detail short exos all latexmkbuild
