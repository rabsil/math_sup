\chapter{Exemples de raisonnements erronés classiques}\label{ann:bad-sols}

Ce chapitre illustre des solutions délibérément erronées de plusieurs exercices de ce document. Ces raisonnements faux sont fournis à titre pédagogique, car ils illustrent des exemples classiques d'erreurs commises par les étudiants.

L'erreur ci-dessous illustre les raisonnement incorrects habituellement suivis par les étudiants dans la compréhension de la disjonction (le «~ou~») et de l'implication.

\begin{error}\label{err:bad-or-then}
Construisez la table de vérité de la proposition $(p \vee q) \then r$.

On construit la table de vérité de cette proposition comme suit.

\begin{table}[!htp]
\centering
\begin{tabular}{ccccc}
$p$ & $q$ & $r$ & $p \vee q$ & $(p \vee q) \then r$\\
\hline
$V$ & $V$ & $V$ & \textcolor{red}{$F$} & \textcolor{red}{$F$}\\
$V$ & $V$ & $F$ & \textcolor{red}{$F$} & \textcolor{red}{$F$}\\
$V$ & $F$ & $V$ & $V$ & $V$\\
$F$ & $V$ & $V$ & $V$ & $V$\\
$V$ & $F$ & $F$ & $V$ & $F$\\
$F$ & $V$ & $F$ & $V$ & $F$\\
$F$ & $F$ & $V$ & $V$ & $V$\\
$F$ & $F$ & $F$ & $V$ & $F$\\
\hline
\end{tabular}
\caption{Table de vérité de $(p \vee q) \then r$}
\end{table}

Plusieurs erreurs, en rouge, apparaissent dans la construction de cette table :
\begin{itemize}
\item souvent, les étudiants pensent que $V \vee V$ est faux, or ce n'est pas le cas,
\item souvent, les étudiants pensent que $F \then V$ est faux, ainsi que $F \then F$, or ce n'est pas le cas.
\end{itemize}

Pour rappel,
\begin{itemize}
\item une disjonction («~ou~», noté $\vee$) est fausse uniquement quand ses deux opérandes sont fausses,
\item une implication est fausse uniquement si le premier opérande est vrai et le second faux.
\end{itemize}
\end{error}

Souvent, les étudiants ont du mal à nier des conjonctions, des disjonctions et des implications. L'erreur ci-dessous illustre les fautes couramment commises dans la négation de ces propositions.

\begin{error}
Niez en français
\begin{enumerate}
\item «~un brasseur achète de l'orge et du houblon~»,
\item «~je vais m'entraîner le mardi ou le vendredi~»,
\item «~si je mange trop de frites, alors je vais grossir~».
\end{enumerate}

Procédons au cas par cas.
\begin{enumerate}
\item On peut modéliser cette proposition comme $p \wedge q$, avec $p : $ «~un brasseur achète de l'orge~» et $q : $ «~un brasseur achète du houblon~». La négation de cette proposition est donc $\lnot p \wedge \lnot q$, c'est-à-dire «~le brasseur n'achète pas d'orge et pas de houblon~».

Or, si l'on réfléchit, si un brasseur doit acheter à la fois de l'orge et du houblon, les cas dans lesquels il est en défaut sont celui où au moins l'un des deux ingrédient est manquants, c'est-à-dire s'il n'achète \emph{pas} de bière \emph{ou} \emph{pas} de houblon, comme décrit par les lois de De Morgan :
\begin{equation*}
\lnot(p \wedge q) \ssi \lnot p \vee \lnot q.
\end{equation*}
En français, la négation correcte est donc «~le brasseur n'achète pas d'orge ou pas de houblon~».

\item On peut modéliser cette proposition comme $p \vee q$, avec $p : $ «~je vais m'entraîner le mardi~» et $q : $ «~je vais m'entraîner le vendredi~». La négation de cette proposition est donc $\lnot p \vee \lnot q$, c'est-à-dire «~je ne vais pas m'entraîner le mardi ou pas le vendredi~».

Notons que cette solution interdit de s'entraîner l'un des deux jours. Or, de nouveau, si l'on réfléchit, si l'on affirme s'entraîner le mardi ou le vendredi, l'unique cas où l'on ment est celui où on ne va ni s'entraîner le mardi, ni le vendredi, comme décrit par les lois de De Morgan :
\begin{equation*}
\lnot(p \vee q) \ssi \lnot p \wedge \lnot q.
\end{equation*}
En français, la négation correcte est donc «~je vais pas m'entraîner le mardi, et pas le vendredi non plus~» (ou encore, «~je ne vais m'entraîner ni le mardi, ni le vendredi~»).
\end{enumerate}

\item On peut modéliser cette proposition comme $p \then q$, avec $p : $ «~je mange trop de frites~» et $q : $ «~je vais grossir~». La négation de cette proposition est donc $\lnot p \then \lnot q$, c'est-à-dire «~si je ne mange pas trop de frites, alors je ne vais pas grossir~».

Or, le seul cas dans lequel on ment dans cette proposition originale est si l'on mange trop de frites \emph{et} qu'on ne grossit pas, ce qui a un sens différent de la solution exprimée ci-dessus. Pour rappel, 
\begin{equation*}
\lnot (p \then q) \ssi p \wedge \lnot q.
\end{equation*}
En français, la négation correcte est donc «~je mange trop de frites et je ne vais pas grossir~».
\end{error}

L'erreur ci-dessous est souvent commise dans le calcul de domaine de fonctions.

\begin{error}
Calculez le domaine et l'image de $f : \IR \mapsto \IZ, x \mapsto x^2$.

Clairement, $\Imm{f} = \IZ^+$, car aucun carré ne peut être négatif. Par ailleurs, comme $x^2$ est évaluable sur tout $\IR$, on a $\Dom{f} = \IR$.

Néanmoins, le réflexe de considérer le domaine comme un ensemble d'entrées que l'on a le droit de mathématiquement introduire dans une formule est erroné. Il est bel et bien possible de calculer $\left(\frac{3}{2}\right)^2$, qui donne $\frac{9}{4}$, néanmoins $\frac{9}{4} \notin \IZ$ ! Pour rappel, le domaine est l'ensemble des points de l'ensemble de départ qui ont une image dans l'ensemble d'arrivée, et $\frac{3}{2} \in \IR$ n'a pas d'image dans $\IZ$.

Comme seuls les entiers peuvent avoir une image entière, le domaine de $f$ est $\IZ$, et non $\IR$.
\end{error}

L'erreur ci-dessous illustre des fautes classiques de modélisation de prédicat quantifié.

\begin{error}\label{err:bad-pred-mod}
Nier et modélisez en français la proposition « tous les serpents ne sont pas venimeux ».

Soient $s$ la variable modélisant un serpent, et $v(s)$ le prédicat « le serpent $s$ est venimeux », défini sur l'ensemble des serpents. La proposition ci-dessus peut-être modélisée comme $\forall s, \lnot v(s)$.

Dès lors, sa négation mathématique est $\exists s, v(s)$, c'est-à-dire « il existe un serpent venimeux ». Ce n'est, à l'évidence pas le contraire de la proposition originale.

L'erreur provient du fait que lorsque que l'on modélise cette proposition comme $\forall s, \lnot v(s)$, on ne dit pas « tous les serpents ne sont pas venimeux », mais « quel que soit le serpent que je considère, ce serpent n'est pas venimeux », ou en d'autres termes « aucun serpent n'est venimeux ». Cette dernière proposition n'a clairement pas le sens de la proposition originale.

On peut modéliser correctement cette proposition par $\exists s, \lnot v(s)$, c'est-à-dire « il existe un serpent qui n'est pas venimeux », ou en d'autres termes « tous les serpents ne sont pas venimeux ». Dès lors, la négation de cette proposition est construite comme $\forall s, v(s)$, c'est-à-dire « tous les serpents sont venimeux ».
\end{error}

L'erreur ci-dessous illustre les problèmes de calculs qui peuvent découler d'une modélisation incorrecte dans le cas de comptage.

\begin{error}[Exercice \ref{ex:chess}, pt. 1]\label{err:chess}
Comptons le nombre de placements de $8$ pions possibles sur un échiquier de 64 cases. Modélisons un placement de pions comme un vecteur
\begin{equation*}
p = (p_1, p_2,\dots,p_{64}),
\end{equation*}
avec
\begin{equation*}
p_i = \left\{
\begin{array}{cl}
1 & \textrm{ si la case } i \textrm{ est occupée},\\
0 & \textrm{sinon}.
\end{array}
\right.
\end{equation*}
Avec cette modélisation, on remarque que l'ordre importe, sans la mesure où $(1,0,\dots) \neq (0,1,\dots)$. Dans le premier cas, on aurait un pion à la case $1$ et pas à la case $2$, et l'inverse dans le second cas. De la même manière, on ne peut placer qu'un pion par case, les répétitions sont donc interdites, et on a $A^8_{64}$ possibilités de placement possibles.

L'erreur dans ce type de raisonnement est que considérer les arrangements de cette manière distingue l'ordre dans lequel les pions sont placés, ce qui est sans importance pour le problème. Aussi, à chaque tel positionnement, il y a exactement $8!$ positionnements similaires : les $8!$ réarrangements de l'ordre dans lequel les pions ont été placés. Par la règle de la division, on a donc $\frac{A^8_{64}}{8!} = C^8_{64}$ placements possibles, ce qui correspond à la réponse attendue.
\end{error}

L'erreur ci-dessous illustre un problème classique en combinatoire : le surcomptage. 

%TODO : simpler example

\begin{error}[Exercice \ref{exo-count-bin} - 4]\label{err:bad-counting}
Reprenons au calcul du nombre de chaînes qui contiennent un bloc de trois «~\texttt{0}~» consécutifs et un bloc de quatre « \texttt{1} » consécutifs. La construction détaillée ci-après est illustrée à la Figure \ref{fig:exo-binblock2}.

Il y a initialement $8$ positions possibles pour le bloc de trois « \texttt{0} ». Ensuite, le bloc de quatre « 1 » peut se positionner à maximum quatre indices possibles. 

Plus particulièrement, si le bloc « \texttt{000} » se trouve à la position
\begin{itemize}
\item $1$, les blocs « \texttt{1111} » peuvent se placer aux positions $4$, $5$, $6$ et $7$;
\item $2$, les blocs « \texttt{1111} » peuvent se placer aux positions $5$, $6$ et $7$;
\item $3$, les blocs « \texttt{1111} » peuvent se placer aux positions $6$ et $7$;
\item $4$, les blocs « \texttt{1111} » peuvent se placer uniquement à la position $7$;
\item $5$, les blocs « \texttt{1111} » peuvent se placer uniquement à la position $1$;
\item $6$, les blocs « \texttt{1111} » peuvent se placer aux positions $1$ et $2$;
\item $7$, les blocs « \texttt{1111} » peuvent se placer aux positions $1$, $2$, $3$;
\item $8$, les blocs « \texttt{1111} » peuvent se placer aux positions $1$, $2$, $3$ et $4$.\\
\end{itemize}

Enfin, les trois positions libres dans la chaîne peuvent prendre des valeurs arbitraires. Il y a $\alpha^3_2 = 2^3 = 8$ telles possibilités pour ces caractères.

Ainsi, en s'aidant de la Figure \ref{fig:exo-binblock2} pour compter le nombre de chaînes qui contiennent un bloc de trois «~\texttt{0}~» consécutifs et un bloc de quatre « \texttt{1} » consécutifs, on remarque qu'on a exactement
\begin{equation*}
4 \cdot 8 + 3 \cdot 8 + 2 \cdot 8 + 1 \cdot 8 + 1 \cdot 8 + 2 \cdot 8 + 3 \cdot 8 + 4 \cdot 8 = 160
\end{equation*}
telles chaînes. Ainsi, par la règle de la soustraction, on a $1028 + 252 - 160 - 160 = 960$ chaînes de chaînes de caractères binaires de longueur $10$ qui contiennent soit trois « \texttt{0} » consécutifs, soit quatre « \texttt{1} » consécutifs.

\begin{figure}[!htp]
\begin{center}
\begin{tikzpicture}[rotate=-90,scale=.8]
\coordinate (root) at (9.2,-8);

\node[rounded corners = 3pt, draw] (p1) at (0,-2) {\includegraphics[scale=.4]{pics/bin1}};
\draw (root) -- (p1.west);

\node[rounded corners = 3pt, draw] (p2) at (3,-2) {\includegraphics[scale=.4]{pics/bin2}};
\draw (root) -- (p2.west);

\node[rounded corners = 3pt, draw] (p3) at (5.75,-2) {\includegraphics[scale=.4]{pics/bin3}};
\draw (root) -- (p3.west);

\node[rounded corners = 3pt, draw] (p4) at (8.15,-2) {\includegraphics[scale=.4]{pics/bin4}};
\draw (root) -- (p4.west);

\node[rounded corners = 3pt, draw] (p5) at (10.25,-2) {\includegraphics[scale=.4]{pics/bin5}};
\draw (root) -- (p5.west);

\node[rounded corners = 3pt, draw] (p6) at (12.65,-2) {\includegraphics[scale=.4]{pics/bin6}};
\draw (root) -- (p6.west);

\node[rounded corners = 3pt, draw] (p7) at (15.45,-2) {\includegraphics[scale=.4]{pics/bin7}};
\draw (root) -- (p7.west);

\node[rounded corners = 3pt, draw] (p8) at (18.45,-2) {\includegraphics[scale=.4]{pics/bin8}};
\draw (root) -- (p8.west);

\foreach \i/\j/\k in {1/0/0, 2/3/2, 3/5.75/0, 4/8.15/3, 5/10.25/0, 6/12.65/2, 7/15.45/0, 8/18.45/2}
{
	\node (bin\i1) at (\j-1.225,3+\k) {\footnotesize \texttt{000}};
	\node (bin\i2) at (\j-0.875,3+\k) {\footnotesize \texttt{100}};
	\node (bin\i3) at (\j-0.525,3+\k) {\footnotesize \texttt{010}};
	\node (bin\i4) at (\j-0.175,3+\k) {\footnotesize \texttt{001}};
	\node (bin\i5) at (\j+0.175,3+\k) {\footnotesize \texttt{110}};
	\node (bin\i6) at (\j+0.525,3+\k) {\footnotesize \texttt{101}};
	\node (bin\i7) at (\j+0.875,3+\k) {\footnotesize \texttt{011}};
	\node (bin\i8) at (\j+1.225,3+\k) {\footnotesize \texttt{111}};
}

\foreach \i in {1, ..., 8}
{
	\foreach \j in {1, ..., 8}
	{
		\draw (p\i.east) -- (bin\i\j.west);
	}
}

\end{tikzpicture}
\caption{Construction des chaînes qui contiennent trois « \texttt{0} » consécutifs et quatre « \texttt{1} » consécutifs}
\label{fig:exo-binblock2}
\end{center}
\end{figure}

Cette décomposition semble « simple » et intuitive, mais est erronée, à cause d'une erreur de surcomptage. En effet, si l'on considère la première bulle de la Figure \ref{fig:exo-binblock2}, la première ligne compte entre autres la chaîne « \texttt{0001111100} », et la deuxième ligne compte également entre autres cette chaîne.

\end{error}

L'erreur ci-dessous illustre le genre de problèmes que l'on peut rencontrer en manipulant de manière imprudente une notation travaillant avec des quantités « infinies ».

%\begin{error}[Manipulation de sommes infinies]\label{err:bad-infinite}
%Soit $S = \displaystyle \sum_{n=0}^\infty (-1)^n = 1 - 1 + 1 - 1 + 1 - 1 \dots$ Cette série ne converge pas, car le terme général de la suite sous-jacente ne converge pas vers zéro (cf. Définition \ref{def:conv-serie}). 
%
%Néanmoins, si l'on est peu prudent, on peut notamment montrer, \emph{à tort}, que $S = \frac{1}{2}$.
%
%On ne peut pas conclure rapidement que cette série est égale à $1$ « si on s'arrête sur un $n$ pair », et $0$ « si on s'arrête sur un $n$ impair », car une série est définie comme une somme \emph{infinie} : « s'arrêter » n'est donc pas un argument pertinent.
%
%Utilisons un artifice de calcul similaire à ce qui est utilisé dans la preuve de la Propriété~\ref{prop:conv-serie-geom}. On a 
%\begin{align*}
%	  & S = 1 - 1 + 1 - 1 + 1 \dots & \\
%\ssi  & S = 1 - (\underbrace{1 - 1 + 1 - 1 + 1 \dots}_{= S}) & \textrm{ par simple placement de parenthèses}\\ 
%\ssi  & S = 1 - S\\
%\ssi  & 2S = 1\\
%\ssi  & S = \frac{1}{2}
%\end{align*}
%
%L'argument utilisé ci-dessus est « j'ai le droit de faire ceci, car les suites dans les parenthèses sont les mêmes ». Cet argument est invalide. En effet, en notant les termes de cette série comme $x_1, x_2, x_3, \dots$, avec le même argument, on peut dire que
%\begin{align*}
%S &= x_1 + x_2 + x_3 + x_4 + x_5 + x_6 + x_7 + x_8 + \dots\\
%  &= (x_1 + x_2 + x_5 + x_6 + \dots) + (x_3 + x_4 + x_7 + x_8 + \dots)\\
%  &= (\underbrace{1 - 1 + 1 - 1 + \dots}_{= S}) + (\underbrace{1 - 1 + 1 - 1 + \dots}_{= S})
%\end{align*}
% On a donc $S = 2S$. Comme avec le résultat précédent, on a $S=\frac{1}{2}$, on peut diviser par $S$. Ainsi, on obtient $1 = 2$. Ce résultat est évidemment absurde, et faux.
% 
%Une raison plus systématique pour lequel ce résultat est faux est le suivant : la manipulation d'une série comme un nombre « classique » (addition, soustraction, etc.) n'est correcte \emph{que} si cette série converge. La notation $S = \displaystyle \sum_{i= 1}^{+\infty} x_i$ a donc un sens \emph{uniquement} quand la somme infinie est bien définie, c'est-à-dire quand elle converge.
%
%Notez qu'il existe d'autres définitions de convergences que celles vues dans ce cours, qui ont du sens pour les gens qui les utilisent, comme en physique, et donc pour lesquels cette série valant $\frac{1}{2}$ a bel et bien un sens. Ce n'est pas notre cas.
%\end{error}