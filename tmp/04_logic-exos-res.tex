\chapter{Algèbre booléenne et logique mathématique}\label{chap:logic}

La plupart des énoncés de ces exercices ci-dessous sont tirés du livre de Rosen~\cite{rosen}.

\begin{exo}
Lesquelles de ces phrases sont-elles des propositions ?
\begin{itemize}
\item Bruxelles est la capitale de la Belgique,
\item $2+2=5$,
\item il existe de la vie ailleurs dans l'univers,
\item répondez à cette question.
\end{itemize}
\end{exo}

\begin{sol}
La première phrase est bien une proposition : elle déclare un fait, et a une valeur de vérité, en l'occurrence vraie.

La deuxième « phrase » est également une proposition, qui affirme le fait que deux et deux fassent cinq. Elle a également une valeur de vérité, qui dans ce cas-ci est fausse.

La troisième phrase est également une proposition : elle déclare en effet un fait, et possède aussi une valeur de vérité, soit vraie, soit fausse. Qu'on ne puisse pas déterminer, voire même deviner laquelle des deux est avérée à l'heure actuelle est sans importance : on sait que c'est exactement une de ces possibilités.

La quatrième phrase n'est par contre pas une proposition : elle ne déclare pas de fait, et ne possède pas de valeur de vérité.
\end{sol}

\begin{exo}
Écrivez la négation des phrases suivantes, sans utiliser de formulation telle que « il est faux que » ou équivalents.
\begin{itemize}
\item $2+2=4$.
\item J'ai un smartphone.
\item Il n'y a pas de pollution en Belgique.
\item Si je plonge dans la piscine, je serai mouillé.
\item Je vais jouer à l'ordinateur ou réussir l'examen.
\item L'été en Écosse n'est ni chaud, ni ensoleillé.
\end{itemize}
\end{exo}

\begin{sol}
Les négations de ces phrases sont construites de la façon suivante.
\begin{itemize}
\item La négation de « deux plus deux égal quatre » est « deux plus deux n'est pas égal à quatre », ou en écriture mathématique : $2+2\neq 4$.
\item Nier le fait que l'on ait un smartphone revient à dire que l'on n'a pas de smartphone.
\item Nier « il n'y a pas de pollution en Belgique » revient simplement à dire « il y a de la pollution en Belgique ». 
\item Cette phrase étant plus difficile, passons par une étape de modélisation. Dans le cas général, cette étape est systématiquement nécessaire, afin ne fusse que de justifier la réponse donnée à l'exercice, ou simplement de pas tomber dans des contre-intuitions inhérentes au langage. Soient $p$ la proposition « je joue à l'ordinateur » et $q$ « je réussis l'examen\footnote{Notez que dans cette phrase, les temps de conjugaison des propositions originales ont changé. Ce changement est sans importance pour la modélisation ou pour la valeur de vérité des propositions, et est simplement effectué pour rendre la lecture plus naturelle en français.} ». On doit donc nier $p \vee q$, ce qui donne $\lnot p \wedge \lnot q$ (par les lois de De Morgan), ou en français : « je ne joue pas à l'ordinateur et je réussis pas l'examen ».
\item  De la même manière que précédemment, modélisons en mathématiques la phrase à nier. Soient $p$ la proposition « je plonge dans la piscine » et $q$ la proposition « je suis mouillé~». On doit nier $p \then q$. Dans la mesure où ce cas-ci est un peu plus difficile, écrivons la table de vérité de cette proposition. Notez qu'en général, cette étape « table de vérité » sera systématiquement nécessaire, les constructions des négations n'étant pas immédiates. \\

\begin{center}
\begin{tabular}{cccc}
\hline
$p$ & $q$ & $p \then q$ & $\lnot(p \then q)$\\
\hline
$F$ & $F$ & $V$ & $F$\\
$F$ & $V$ & $V$ & $F$\\
$V$ & $F$ & $F$ & $V$\\
$V$ & $V$ & $V$ & $F$\\
\hline
\end{tabular}
\end{center}

\vspace{0.5cm} %yeah I know it's ugly...

%\begin{center}
%\begin{tikzpicture}
%\node (table) {
%\begin{tabular}{cccc}
%\hline
%$p$ & $q$ & $p \then q$ & $\lnot(p \then q)$\\
%\hline
%$F$ & $F$ & $V$ & $F$\\
%$F$ & $V$ & $V$ & $F$\\
%$V$ & $F$ & $F$ & $V$\\
%$V$ & $V$ & $V$ & $F$\\
%\hline
%\end{tabular}};
%
%\draw [red,ultra thick,rounded corners]
%  ($(table.south west) !.25! (table.north west)$)
%  rectangle 
%  ($(table.south east) !.4! (table.north east)$);
%\end{tikzpicture}
%\end{center}

La dernière colonne de cette table de vérité a la même valeur de vérité que $p \wedge \lnot q$, ce qui correspond, en français, à « je plonge dans la piscine et je ne suis pas mouillé ».

\item Cette dernière phrase est un peu plus subtile, du fait de la présence de négations déjà au sein de la phrase originale. Soient $p$ la proposition « l'été en Écosse est chaud » et $q$ « l'été en Écosse est ensoleillé ».la proposition à nier est $\lnot p \wedge  \lnot q$, c'est-à-dire décrire la valeur de vérité de $\lnot \big( \lnot p \wedge  \lnot q \big)$. Écrivons la table de vérité de cette proposition\footnote{Remarquez qu'un étudiant « à l'aise » avec la matière pourrait simplement distribuer la négation et trouver directement la solution via les lois de De Morgan, sous réserve qu'il justifie sa réponse correctement.} :\\

\begin{center}
\begin{tabular}{cccccc}
\hline
$p$ & $q$ & $\lnot p$ & $\lnot q$ & $\lnot p \wedge  \lnot q$ & $\lnot \big(
\lnot p \wedge  \lnot q \big)$\\
\hline
$F$ & $F$ & $V$ & $V$ & $V$ & $F$ \\
$F$ & $V$ & $V$ & $F$ & $F$ & $V$ \\
$V$ & $F$ & $F$ & $V$ & $F$ & $V$ \\
$V$ & $V$ & $F$ & $F$ & $F$ & $V$ \\
\hline
\end{tabular}
\end{center}

\vspace{0.5cm} %you didn't see anything

On remarque que la dernière colonne de cette table correspond à $p \vee q$. La négation de la phrase originale est donc « l'été en Écosse est chaud ou ensoleillé ».
\end{itemize}
\end{sol}

\begin{exo}
Soient $p$ et $q$ les propositions suivantes :
\begin{itemize}
\item $p$ : j'achète un billet de loterie ce week-end,
\item $q$ : je gagne un jackpot d'un million d'euros.\\
\end{itemize}
Exprimez chacune des propositions suivantes en français :
\begin{multicols}{3}
\begin{enumerate}
\item $\lnot p$,
\item $p \vee q$,
\item $p \then q$,
\item $q \then p$,
\item $p \wedge q$,
\item $p \ssi q$,
\item $\lnot p \then \lnot q$,
\item $\lnot p \wedge \lnot q$,
\item $\lnot p \vee (p \wedge q)$.
\end{enumerate}
\end{multicols}
\end{exo}

\begin{sol} 
On peut écrire ces propositions de la façon suivante :
\begin{enumerate}
\item Je n'achète pas de billet de loterie ce week-end.
\item J'achète un billet de loterie ce week-end ou je gagne un jackpot d'un million d'euros. 
%\item Si j'avais acheté billet de loterie, alors j'aurais gagné un jackpot d'un
%million d'euros\footnote{On peut conjuguer cette phrase également avec une
%combinaison indicatif présent / futur simple.}.
\item Si j'achète un billet de loterie ce week-end, alors je gagne un jackpot d'un million d'euros.
\item Si je gagne un jackpot d'un million d'euros, alors j'achète un billet de loterie ce week-end.
\item J'achète un billet de loterie ce week-end et je gagne un jackpot d'un millions d'euros.
\item J'achète un billet de loterie ce week-end si et seulement si je gagne un jackpot d'un million d'euros.
\item Si je n'achète pas de billet de loterie ce week-end, alors je ne gagne pas de jackpot d'un million d'euros.
\item Je n'achète pas de billet de loterie ce week-end et je ne gagne pas de jackpot d'un million d'euros.
\item Je n'achète pas de billet de loterie ce week-end, ou j'achète un billet de loterie ce week-end et je gagne un jackpot d'un million d'euros.
\end{enumerate}
\end{sol}

\begin{exo}
Soient $p$, $q$ et $r$ les propositions suivantes.
\newcommand{\exogrizzli}{des grizzlis n'ont pas été vus dans les environs}
\newcommand{\exorando}{faire de la randonnée sur ce sentier est sans danger}
\newcommand{\exobaies}{les baies sont mûres sur ce sentier}
\begin{itemize}
\item[$p$] : des grizzli ont été vus dans les environs.
\item[$q$] : \exorando.
\item[$r$] : \exobaies.\\
\end{itemize}
Écrivez les propositions suivantes en mathématiques en utilisant des connecteurs logiques :
\begin{enumerate}
\item \exobaies, mais \exogrizzli;
\item \exogrizzli et \exorando, mais \exobaies;
\item si \exobaies, y faire de la randonnée est sans danger si et seulement si
\exogrizzli;
\item il est dangereux de faire de la randonnée sur ce sentier, mais
\exogrizzli\ et \exobaies;
\item pour faire de la randonnée sans danger sur ce sentier, il est nécessaire mais pas suffisant que les baies soient mûres sur ce sentier et que l'on n'ait pas vu de grizzlis dans les environs.
\item \exorando\ quand \exogrizzli\ et quand \exobaies.
\end{enumerate}
\end{exo}

\begin{sol}
Ces propositions peuvent être écrites en mathématiques de la façon suivante.
\begin{enumerate}
\item $r \wedge \lnot p$. Le « mais » a la même valeur logique que le « et ».
\item $\lnot p \wedge q \wedge r$. Même justification.
\item $r \then (q \ssi \lnot p)$. L'implication est claire, et le conséquent est formé de deux parties équivalentes (« si et seulement si »). Notez ici l'importance des parenthèses, à cause des priorités des opérateurs.
\item $\lnot q \wedge \lnot p \wedge r$. Même justification qu'au premier point. Être dangereux est le contraire d'être sans danger, d'où le $\lnot q$.
\item $q \then (r \wedge \lnot p)$. Cette phrase dénote une implication. De plus, pour rappel, dans une implication $x \then y$, $y$ est appelé une condition nécessaire pour $p$.
\item $(\lnot p \wedge r) \then q$. Cette phrase dénote également une implication. Le conséquent de cette implication est la randonnée dangereuse.
\end{enumerate}
\end{sol}

\begin{exo}
Déterminez la valeur de vérité des propositions suivantes.
\begin{itemize}
\item $1 + 1 = 2 \ssi 2 + 2 = 4$.
\item $1 + 1 = 3 \ssi 2 + 2 = 4$.
\item $1 + 1 = 3$ si et seulement si les vaches peuvent voler. 
\item Si les vaches peuvent voler, alors $1 + 1 = 3$.
\item Si les vaches peuvent voler, alors $1 + 1 = 2$.
\end{itemize}
\end{exo}

\begin{sol}
Les valeurs de vérité des propositions sont les suivantes.
\begin{itemize}
\item Vrai : les sous-propositions sont équivalentes, toutes les deux ont la même valeur de vérité vrai.
\item Faux : les sous-propositions ne sont pas équivalentes. En effet, $1 + 1 = 3$ a la valeur de vérité faux, mais $2 + 2 = 4$ a la valeur de vérité vrai.
\item Vrai : les sous-propositions sont équivalentes, toutes les deux ont la même valeur de vérité faux.
\item Vrai : toute implication d'une proposition par une autre qui est fausse (les vaches peuvent voler) est vraie.
\item Vrai : même justification que ci-dessus.
\end{itemize}
\end{sol}

\begin{exo}\label{exo:tables}
Construisez les tables de vérité des propositions suivantes.
\begin{multicols}{2}
\begin{enumerate}
\item $p \then \lnot p$,
\item $p \ssi \lnot p$,
\item $p \xor (p \vee q)$,
\item $(p \vee q) \then (p \wedge q)$,
\item $(p \then q) \ssi (\lnot q \then \lnot p)$,
\item $(p \then q) \then (q \then p)$.
\end{enumerate}
\end{multicols}
\end{exo}

\begin{sol}
Les tables de vérité de ces propositions sont illustrées à la figure \ref{fig:exotables}. Notez que le point 2 est une antilogie triviale (car $p$ et $\lnot p$ n'ont jamais la même valeur de vérité), et que le point $5$ est une tautologie définie dans le cours. Notez également que le point 6 n'est pas équivalent à $p \ssi q$.
\end{sol}

%figure code is ugly, but better looking result than with subfigure (see below)
\begin{figure}[!htp]
\begin{center}
\begin{minipage}{.4\textwidth}
\begin{center}
Exercice 1\\[0.5em]
\begin{tabular}{ccc}
\hline
$p$ & $\lnot p$ & $p \then \lnot p$\\
\hline
$F$ & $V$ & $V$\\
$V$ & $F$ & $F$\\
\hline
\end{tabular}
\end{center}
\end{minipage}
\begin{minipage}{.4\textwidth}
\begin{center}
Exercice 2\\[0.5em]
\begin{tabular}{ccc}
\hline
$p$ & $\lnot p$ & $p \ssi \lnot p$\\
\hline
$F$ & $V$ & $F$\\
$V$ & $F$ & $F$\\
\hline
\end{tabular}
\end{center}
\end{minipage}\vspace{0.5cm}
\begin{minipage}{.3\textwidth}
\begin{center}
Exercice 3\\[0.5em]
\begin{tabular}{cccc}
\hline
$p$ & $q$ & $p \vee q$ & $p \xor (p \vee q)$\\
\hline
$F$ & $F$ & $F$ & $F$\\
$F$ & $V$ & $V$ & $V$\\
$V$ & $F$ & $V$ & $F$\\
$V$ & $V$ & $V$ & $F$\\
\hline
\end{tabular}
\end{center}
\end{minipage}\hspace{1cm}
\begin{minipage}{.6\textwidth}
\begin{center}
Exercice 4\\[0.5em]
\begin{tabular}{ccccc}
\hline
$p$ & $q$ & $p \vee q$ & $p \wedge q$ & $(p \vee q) \then (p \wedge q)$\\
\hline
$F$ & $F$ & $F$ & $F$ & $V$\\
$F$ & $V$ & $V$ & $F$ & $F$\\
$V$ & $F$ & $V$ & $F$ & $F$\\
$V$ & $V$ & $V$ & $V$ & $V$\\
\hline
\end{tabular}
\end{center}
\end{minipage}\vspace{0.5cm}
\begin{minipage}{.9\textwidth}
\begin{center}
Exercice 5\\[0.5em]
\begin{tabular}{ccccccc}
\hline
$p$ & $q$ & $p \then q$ & $\lnot q$ & $\lnot p$ & $\lnot q \then \lnot p$ & $(p
\then q) \ssi (\lnot q \then  \lnot p)$\\
\hline
$F$ & $F$ & $V$ & $V$ & $V$ & $V$ & $V$\\
$F$ & $V$ & $V$ & $F$ & $V$ & $V$ & $V$\\
$V$ & $F$ & $F$ & $V$ & $F$ & $F$ & $V$\\
$V$ & $V$ & $V$ & $F$ & $F$ & $V$ & $V$\\
\hline
\end{tabular}
\end{center}
\end{minipage}\vspace{0.5cm}
\begin{minipage}{.9\textwidth}
\begin{center}
Exercice 6\\[0.5em]
\begin{tabular}{ccccc}
\hline
$p$ & $q$ & $p \then q$ & $q \then p$ & $(p \then q) \then (q \then p)$\\
\hline
$F$ & $F$ & $V$ & $V$ & $V$\\
$F$ & $V$ & $V$ & $F$ & $F$\\
$V$ & $F$ & $F$ & $V$ & $V$\\
$V$ & $V$ & $V$ & $V$ & $V$\\
\hline
\end{tabular}
\end{center}
\end{minipage}
\caption{tables de vérité de d'Exercice \ref{exo:tables}}
\label{fig:exotables}
\end{center}
\end{figure}

%\begin{figure}[!htp]
%\begin{center}
%\subfigure[Exercice 1]{
%\begin{tabular}{ccc}
%\hline
%$p$ & $\lnot p$ & $p \then \lnot p$\\
%\hline
%$F$ & $V$ & $V$\\
%$V$ & $F$ & $F$\\
%\hline
%\end{tabular}}
%\subfigure[Exercice 2]{
%\begin{tabular}{ccc}
%\hline
%$p$ & $\lnot p$ & $p \ssi \lnot p$\\
%\hline
%$F$ & $V$ & $F$\\
%$V$ & $F$ & $F$\\
%\hline
%\end{tabular}}
%\subfigure[Exercice 3]{
%\begin{tabular}{cccc}
%\hline
%$p$ & $q$ & $p \vee q$ & $p \xor (p \vee q)$\\
%\hline
%$F$ & $F$ & $F$ & $F$\\
%$F$ & $V$ & $V$ & $V$\\
%$V$ & $F$ & $V$ & $F$\\
%$V$ & $V$ & $V$ & $F$\\
%\hline
%\end{tabular}}
%\subfigure[Exercice 4]{
%\begin{tabular}{ccccc}
%\hline
%$p$ & $q$ & $p \vee q$ & $p \wedge q$ & $(p \vee q) \then (p \wedge q)$\\
%\hline
%$F$ & $F$ & $F$ & $F$ & $V$\\
%$F$ & $V$ & $V$ & $F$ & $F$\\
%$V$ & $F$ & $V$ & $F$ & $F$\\
%$V$ & $V$ & $V$ & $V$ & $V$\\
%\hline
%\end{tabular}}
%\subfigure[Exercice 5]{
%\begin{tabular}{ccccccc}
%\hline
%$p$ & $q$ & $p \then q$ & $\lnot q$ & $\lnot p$ & $\lnot q \then \lnot p$ & $(p
%\then q) \ssi (\lnot q \then  \lnot p)$\\
%\hline
%$F$ & $F$ & $V$ & $V$ & $V$ & $V$ & $V$\\
%$F$ & $V$ & $V$ & $F$ & $V$ & $V$ & $V$\\
%$V$ & $F$ & $F$ & $V$ & $F$ & $F$ & $V$\\
%$V$ & $V$ & $V$ & $F$ & $F$ & $V$ & $V$\\
%\hline
%\end{tabular}}
%\subfigure[Exercice 6]{
%\begin{tabular}{ccccc}
%\hline
%$p$ & $q$ & $p \then q$ & $q \then p$ & $(p \then q) \then (q \then p)$\\
%\hline
%$F$ & $F$ & $V$ & $V$ & $V$\\
%$F$ & $V$ & $F$ & $F$ & $F$\\
%$V$ & $F$ & $F$ & $V$ & $F$\\
%$V$ & $V$ & $V$ & $V$ & $V$\\
%\hline
%\end{tabular}}
%\caption{tables de vérité de d'Exercice \ref{exo:tables}}
%\label{fig:exotables}
%\end{center}
%\end{figure}

\begin{exo}
Soit $p(x)$ le prédicat « le mot $x$ contient la lettre 'a' », défini sur l'ensemble des mots de la langue française. Quelles sont les valeurs de vérité de
\begin{itemize}
\item $p(\textrm{orange})$,
\item $p(\textrm{citron})$,
\item $p(\textrm{vrai})$,
\item $p(\textrm{faux})$.
\end{itemize}
\end{exo}

\begin{sol}
Ces propositions ont toutes la valeur de vérité vrai, à l'exception de $p(\textrm{citron})$, car « citron » ne contient pas la lettre $a$.
\end{sol}

\pagebreak %evil

\begin{exo}
Soit $p(x)$ le prédicat $x > 5$. Donnez la valeur de $x$ après qu'une instruction\footnote{Dans cette instruction, le symbole « $:=$ » dénote l'affectation d'une variable à une valeur.} de type « si $p(x)$, alors $x := 1$ », si la valeur de $x$ quand cette instruction est atteinte est
\begin{itemize}
\item $x=0$,
\item $x=1$,
\item $x=6$.
\end{itemize}
\end{exo}

\begin{sol}
L'implication détermine ici la valeur de $x$. Si la valeur de vérité du prédicat évalué est vraie, la valeur de $x$ est changée à $1$, sinon, elle reste à sa valeur originale. On a donc, après exécution :
\begin{itemize}
\item $x = 0$,
\item $x = 1$,
\item $x = 1$.
\end{itemize}
\end{sol}

\begin{exo}
Soit $p(x)$ le prédicat « $x$ passe plus de cinq heures par week-end à travailler », où le domaine de définition de $p$ est l'ensemble des étudiants de la classe. Exprimez les quantifications suivantes en français :
\begin{itemize}
\item $\exists x, p(x)$,
\item $\forall x, p(x)$,
\item $\exists x, \lnot p(x)$,
\item $\forall x, \lnot p(x)$.
\end{itemize}
\end{exo}

\begin{sol}
On peut écrire ces propositions en français de la manière suivante.
\begin{itemize}
\item Il existe un étudiant qui passe plus de cinq heures par week-end à étudier.
\item Tous les étudiants passent plus de cinq heures par week-end à étudier.
\item Il existe un étudiant qui ne passe pas plus de cinq heures à étudier, ou encore il existe un étudiant qui passe moins de cinq heures par week-end à étudier.
\item Tous les étudiants ne passent pas plus de cinq heures à étudier, ou encore tous les étudiants passent moins de cinq heures à étudier, ou enfin aucun étudiant ne passe plus de cinq heures à étudier.
\end{itemize}
\end{sol}

\pagebreak %evil

\begin{exo}
Soient les prédicats suivants, dont le domaine de définition est l'ensemble des étudiants de la classe :
\begin{itemize}
\item $p(x)$ : $x$ a un chat,
\item $q(x)$ : $x$ a un chien,
\item $r(x)$ : $x$ a un hamster.\\
\end{itemize}
Exprimez en mathématiques et sous forme normale les phrases suivantes.
\begin{enumerate}
\item Un étudiant de la classe a un chat, un chien et un hamster.
\item Tous les étudiants de la classe ont un chat, un chien et un hamster.
\item Certains étudiants de la classe ont un chat et un hamster, mais pas de chien.
\item Aucun étudiant de la classe n'a un chat, un chien et un hamster.
\item Aucun étudiant de la classe n'a un chat, un chien ou un hamster.
\item Pour chacun des trois animaux chat, chien et hamster, il y a un étudiant de la classe qui en possède un comme animal de compagnie.
\end{enumerate}
\end{exo}

\begin{sol}
On peut exprimer ces propositions en mathématiques de la façon suivante.
\begin{enumerate}
\item $\exists x, p(x) \wedge q(x) \wedge r(x)$.
\item $\forall x, p(x) \wedge q(x) \wedge r(x)$.
\item $\exists x, p(x) \wedge r(x) \wedge \lnot q(x)$.
\item $\forall x, \lnot (p(x) \wedge q(x) \wedge r(x))$, ou encore, grâce aux lois de De Morgan, $\forall x, \lnot p(x) \vee \lnot q(x) \vee \lnot r(x)$.
\item $\forall x, \lnot (p(x) \vee q(x) \vee r(x))$, ou encore, toujours grâce aux lois de De Morgan, $\forall x, \lnot p(x) \wedge \lnot q(x) \wedge \lnot r(x)$.
\item Dans ce cas-ci, on impose que parmi les étudiants de la classe, il y a au moins un hamster, un chat et un chien comme animal de compagnie. Notez que cela ne signifie pas que ces animaux appartiennent à un unique étudiant. Dès lors, cette proposition est traduite comme $\exists x, \exists y, \exists z, p(x) \wedge q(y) \wedge r(z)$.
\end{enumerate}
\end{sol}

\pagebreak %evil

\begin{exo}
Pour chacun des prédicats suivants, trouvez un domaine de définition pour lequel ce prédicat est vrai, et un pour lequel ce prédicat est faux.
\begin{itemize}
\item Tout le monde étudie les mathématiques discrètes.
\item Tout le monde a plus de $21$ ans.
\item Chaque paire de personnes a la même maman.
\item Aucune paire de personnes n'a la même grand-mère.
\end{itemize}
\end{exo}

\begin{sol}
La table \ref{tab:exopred} illustre des exemples de domaines de définition qui rendent les prédicats vrais ou faux. 

\begin{table}[!htp]
\begin{center}
\begin{tabular}{m{.45\textwidth}|m{.45\textwidth}}
\hline
\textbf{Vrai} & \textbf{Faux}\\
\hline\hline
Un département de recherche en mathématiques discrètes & Le chat de la maison \\
\hline
Les sénateurs belges & Une classe de maternelle \\
\hline
Des frères et sœurs (au moins deux) utérins & Deux personnes qui peuvent se marier légalement en Belgique\\
\hline
Un père et son fils & Des frères et sœurs (au moins deux) utérins \\
\hline
\end{tabular}
\caption{Exemples de domaines de définition}
\label{tab:exopred}
\end{center}
\end{table}
\end{sol}

\begin{exo}
Exprimez les spécifications systèmes suivantes en utilisant des prédicats, des connecteurs logiques et des quantificateurs.
\begin{enumerate}
\item Quand il reste moins de $30$MB d'espace libre sur le disque dur, un message d'avertissement est affiché à l'utilisateur.
\item Aucun répertoire ne peut être ouvert et aucun fichier ne peut être fermé quand des erreurs systèmes ont été détectées.
\item On ne peut pas faire un backup du système quand un utilisateur est logué.
\item La vidéo à la demande peut être fournie quand il reste au moins $128$MB de mémoire disponible et quand la vitesse moyenne de la connexion internet est d'au moins $512$kbps.
\end{enumerate}
\end{exo}

\begin{sol}
On peut modéliser ces phrases de la façon suivante.
\begin{enumerate}
\item Soient $p$ la proposition « il reste au moins $30$MB de libre sur le disque et $q$ la proposition « un message d'avertissement est affiché ». On modélise la spécification comme $p \then q$.
\item Soient $p(x)$ le prédicat « $x$ peut être ouvert », $q(x)$ le prédicat « $x$ peut être fermé », définis sur l'ensemble des fichiers et répertoires, $r(x)$ le prédicat «~$x$ est un fichier\footnote{Pour des raisons de simplicité, on considère ici que tout ce qui n'est pas un fichier est un répertoire.} » ainsi que $s$ la proposition « des erreurs systèmes ont été détectées ». On peut modéliser cette proposition comme 
\begin{equation*}
s \then \forall x, (\lnot r(x) \wedge \lnot p(x)) \wedge (r(x) \wedge \lnot q(x)).
\end{equation*}
\item Soit $p(x)$ le prédicat « $x$ est logué », défini sur tous les utilisateurs, et $q$ la proposition « on peut un backup du système ». On peut modéliser la spécification comme $\exists x, p(x) \then \lnot q$.
\item Soient les propositions $p$ « la vidéo à la demande est fournie », $q$ « il reste au moins $128$MB de mémoire disponible et $r$ « la vitesse de la connexion est d'au moins $512$kbps ». On modélise la spécification comme $(q \wedge r) \then p$.
\end{enumerate}
\end{sol}

\begin{exo}
Soit $p(x,y)$ le prédicat « l'étudiant $x$ aime la cuisine $y$ », défini sur l'ensemble des étudiants de la classe (pour $x$) et sur l'ensemble des cuisines (pour $y$). Exprimez les propositions suivantes en français.
\begin{enumerate}
\item $\lnot p(\textrm{Anne},\textrm{japonaise})$.
\item $\exists y, \Big( p(\textrm{Taric},y) \wedge p(\textrm{Jay},y) \Big)$.
\item $\forall x, \forall z, \exists y, \big( (x \neq z) \then \lnot ( p(x,y) \wedge p(z,y) ) \big)$.
\item $\exists x, \exists z, \forall y, x \neq z \wedge (p(x,y) \ssi p(z,y))$.
\item $\forall x, \forall z, \exists y, x \neq z \wedge (p(x,y) \ssi p(z,y))$.
\end{enumerate}
\end{exo}

\begin{sol}
Les propositions peuvent être exprimées en français de la manière suivante.
\begin{enumerate}
\item Anne n'aime pas la cuisine japonaise.
\item Il existe une cuisine aimée à la fois par Taric et Jay.
\item On remarque ici que $x$ et $z$ sont des étudiants, d'après leur place au sein du prédicat $p$. On obtient donc, en français : « pour toute paire d'étudiants différents, il existe une cuisine aimée par l'un et pas par l'autre ».
\item Il existe deux étudiants différents qui ont les mêmes goûts en cuisine.
\item Toute paire d'étudiants différents possède une cuisine aimée en commun.
\end{enumerate}
\end{sol}

\begin{exo}
Soient les prédicats
\begin{align*}
p(x)  &:& x \textrm{ est un étudiant},\\
q(x)  &:& x \textrm{ est un professeur},\\
r(x,y) &:& x \textrm{ a posé une question à $y$},\\
\end{align*}
définis sur l'ensemble des personnes associées à l'école. Modélisez les phrases suivantes en mathématiques en utilisant des quantificateurs.
\begin{enumerate}
\item Louis a posé une question au professeur Blondiau.
\item Chaque étudiant a posé une question à la directrice.
\item Toutes les personnes associées à l'école ont posé une question soit à la directrice, soit au professeur Blondiau.
\item Certains étudiants n'ont posé de question à personne.
\item Certains professeurs n'ont jamais reçu de question de la part d'étudiants.
\item Il y a une personne associée à l'école qui a posé une question à chaque autre personnes associée à l'école.
\item Certains étudiants n'ont jamais reçu aucune question de n'importe quelle autre personne associée à l'école.
\end{enumerate}
\end{exo}

\begin{sol}
On peut modéliser ces phrases de la façon suivante.
\begin{enumerate}
\item $r(\textrm{Louis},\textrm{Blondiau})$.
\item $\forall x, p(x) \wedge r(x,\textrm{directrice})$.
\item $\forall x, r(x,\textrm{directrice}) \xor r(\textrm{Blondiau},x)$.
\item $\exists x, \forall y, p(x) \wedge \lnot r(x,y)$ ou encore $\forall x, p(x), \forall y, \lnot r(x,y)$.
\item $\exists x, \forall y, q(x) \wedge p(y) \wedge \lnot r(y,x)$ ou encore $\exists x, q(x) \forall y, p(y) \wedge \lnot r(y,x)$.
\item $\exists x, \forall y, x \neq y \wedge r(x,y)$.
\item $\exists x, \forall y, p(x) \wedge x \neq y \wedge \lnot r(y,x)$ ou $\exists x, p(x), \forall y,  x \neq y \wedge \lnot r(y,x)$.
\end{enumerate}
\end{sol}

\begin{exo}\label{ex:neg-pred}
Exprimez la négation des prédicats quantifiés suivants, en mathématiques, et en français.
\begin{enumerate}
\item Tous les étudiants de la classe aiment les mathématiques.
\item Il y a un étudiant de la classe qui n'a jamais vu d'ordinateur.
\item Il y a un étudiant de la classe qui a fait tous les exercices du syllabus.
\item Il y a un étudiant de la classe qui a eu cours à chaque étage.
\end{enumerate}
\end{exo}

\begin{sol}
On formule les négations de ces prédicats de la manière suivante.
\begin{enumerate}
\item Soit $p(x)$ le prédicat « $x$ aime les mathématiques », défini sur l'ensemble des étudiants de la classe. Le prédicat original est modélisé comme $\forall x, p(x)$, et sa négation est $\exists x, \lnot p(x)$, ou en français « il y a un étudiant de la classe qui n'aime pas les mathématiques ».
\item Soit $p(x)$ le prédicat « $x$ a vu un ordinateur », défini sur l'ensemble des étudiants de la classe. Le prédicat original est modélisé comme $\exists x, \lnot p(x)$, et sa négation est $\forall x, p(x)$, ou en français « tous les étudiants de la classe ont vu un ordinateur ».
\item Soient $p(x,y)$ le prédicat « l'étudiant $x$ a fait l'exercice $y$ », défini sur l'ensemble des étudiants de la classe ($x$) et sur l'ensemble des exercices du syllabus ($y$). On modélise le prédicat original comme $\exists x, \forall y, p(x,y)$, sa négation est $\forall x, \exists y, \lnot p(x,y)$, ou en français « aucun étudiant de la classe n'a fait tous les exercices ».
\item Soient $p(x,y)$ le prédicat « l'étudiant $x$ a le cours $y$ » et $q(y,z)$ le prédicat « le cours $y$ est donné à l'étage $z$ », définis sur l'ensemble des étudiants de la classe ($x$), l'ensemble des cours dispensés à l'école ($y$) et l'ensemble des étages du bâtiment ($z$). Le prédicat original est modélisé comme $\exists x, \forall z, \exists y, p(x,y) \wedge q(y,z)$. Notez ici que l'on peut choisir le cours en fonction de chaque étage pour un étudiant donné. En mathématiques, la négation de ce prédicat est $\forall x, \exists z, \forall y, \lnot p(x,y) \vee \lnot q(y,z)$, que l'on obtient à l'aide des règles de négation des prédicats quantifiés et des lois de De Morgan. En français, cela donne donc « pour tout étudiant considéré, il existe un étage tel que quel que soit le cours dispensé à l'école, l'étudiant en question ne suit pas ce cours, ou ce cours n'est pas donné à l'étage en question.\\
\end{enumerate}

%Notez qu'il faut parfois prendre garde à la façon dont on modélise et qu'on nie des prédicats où une négation apparaît déjà, comme le montre l'Erreur \ref{err:bad-pred-mod}.
\end{sol}

\begin{exo}[\!\!\cite{exomathelem}]
Un meurtre a été commis dans la ville de Dualis, séparée en deux par une rivière qui la traverse d'est en ouest. La particularité de cette ville est que seuls les habitants au sud de la rivière disent la vérité. Un inspecteur est dépêché sur place pour résoudre l'affaire, et on lui présente trois témoins, venant des deux parties de la ville : Alex, Virginie et Carl. Il les interroge dans l'espoir de trouver un habitant (du sud de la ville) qui lui dise la vérité sur le meurtre. Voici leurs réponses.
\begin{enumerate}
\item Alex : Virginie habite au sud.
\item Virginie : Alex et moi habitons ensemble.
\item Carl : c'est faux, Virginie ment comme elle respire !\\
\end{enumerate}

Pouvez-vous indiquer quels citoyens l'inspecteur doit interroger à propos du meurtre ?
\end{exo}

\begin{sol}
Modélisons avant tout le problème sous forme mathématique, en commençant par les propositions à analyser. En l'occurrence, on veut savoir qui habite au sud de la ville, pour savoir qui l'inspecteur doit interroger. On pose donc
\begin{align*}
P_1 &:& \textrm{Alex habite au sud de la ville}.\\
P_2 &:& \textrm{Virginie habite au sud de la ville}.\\
P_3 &:& \textrm{Carl habite au sud de la ville}.
\end{align*}
Traduisons à présent leurs affirmations sous forme de propositions logiques. Dans la déposition d'Alex, on déduit les points suivants.
\begin{itemize}
\item Si Alex habite au sud de la ville, il dit la vérité. Or, il affirme que $P_2$ est vrai. Donc, si $P_1$ est vrai, alors $P_2$ est vrai.
\item Si Alex habite au nord, soit il ment, soit il dit la vérité, et on ne peut rien conclure de la valeur de vérité de $P_2$.\\
\end{itemize}

Ces deux affirmations mises ensemble, on en conclut que $P_1 \then P_2$. Ce constat peut être dressé à l'aide de la table de vérité de l'implication. On va procéder de manière similaire pour les dépositions de Virginie et de Carl.

Dans le cas de Virginie, on déduit que si elle habite au sud (si elle dit la vérité, si $P_2$ est vrai), alors elle vit avec Alex, et donc ils habitent tous les deux au sud, ou tous les deux au sud ($P_1 \wedge P_2$), ou tous les deux au nord ($\lnot P_1 \wedge \lnot P_2$). On en déduit donc que $P_2 \then (P_1 \wedge P_2) \vee (\lnot P_1 \wedge \lnot P_2)$. 

Enfin, si Carl vit au sud, alors Virginie ment, et donc ce qu'elle affirme est faux, et donc la négation de ce qu'elle affirme est vraie. On a donc $P_3 \then \lnot\big((P_1 \wedge P_2) \vee (\lnot P_1 \wedge \lnot P_2)\big)$.

Par ailleurs, on sait que les témoins viennent des deux parties de la ville, ce qui signifie que deux habitent au sud et un au nord, ou inversement. On a donc $P_i \wedge P_j \wedge \lnot P_k$ ou l'inverse, c'est-à-dire $\lnot P_i \wedge \lnot P_j \wedge P_k$, pour certains $i,j,k$ différents. Pour cet exercice, il va donc falloir envisager toutes les possibilités de ces valeurs $i,j$ et $k$. 

En conclusion, on obtient donc les propositions suivantes :
\begin{align}
& P_1 \then P_2,\label{eq:police1}\\
& P_2 \then (P_1 \wedge P_2) \vee (\lnot P_1 \wedge \lnot P_2),\label{eq:police2}\\
& P_3 \then \lnot\big((P_1 \wedge P_2) \vee (\lnot P_1 \wedge \lnot P_2)\big),\label{eq:police3}\\
&\begin{array}{l}
	(\lnot P_1 \wedge P_2 \wedge P_3) \vee (P_1 \wedge \lnot P_2 \wedge P_3) \vee (P_1 \wedge P_2 \wedge \lnot P_3) \\
	~~~~~\vee (\lnot P_1 \wedge \lnot P_2 \wedge P_3) \vee (\lnot P_1 \wedge P_2 \wedge \lnot P_3) \vee (P_1 \wedge \lnot P_2 \wedge \lnot P_3).
\end{array}\label{eq:police4}
\end{align}

L'affirmation que les propositions \eqref{eq:police1} à \eqref{eq:police2} est vraie restreint les valeurs de vérité de $P_1$, $P_2$ et $P_3$, et c'est de ces restrictions que l'on va pouvoir aider l'inspecteur. La table de vérité de ces propositions est illustrée à la table \ref{tab:police}. Cette table a été illustrée « horizontalement » plutôt que « verticalement » (comme à l'habitude) pour des raisons de présentation.

La dernière ligne correspondant à la proposition \eqref{eq:police4} peut être obtenue facilement en résonnant de la façon suivante. Toutes les sous-propositions sont séparées par un « ou ». Dès lors, pour rendre cette proposition fausse, il faut que chacune de ces sous-proposition soit fausse. À l'intérieur de ces sous-propositions, toutes les clauses sont séparées par des « et ». Dès lors, pour les rendre fausses, il suffit que l'une des clauses soit fausse. Dans la mesure où on remarque la présence de négations dans chacune de ces sous-propositions, et que tous les cas possibles de une ou deux négations sont envisagés, le seul cas possible est soit quand chacune des clauses $P_1$, $P_2$ et $P_3$ est vraie, soit quand chacune des clauses $P_1$, $P_2$ et $P_3$ est fausse.

\begin{table}[!htp]
\begin{center}
\begin{tikzpicture}
\node (table) {
\begin{tabular}{c||cccccccc}
$P_1$ & $F$ & $F$ & $F$ & $F$ & $V$ & $V$ & $V$ & $V$\\
$P_2$ & $F$ & $F$ & $V$ & $V$ & $F$ & $F$ & $V$ & $V$\\
$P_3$ & $F$ & $V$ & $F$ & $V$ & $F$ & $V$ & $F$ & $V$\\
$P_1 \wedge P_2$ & $F$ & $F$ & $F$ & $F$ & $F$ & $F$ & $V$ & $V$\\
$\lnot P_1 \wedge \lnot P_2$ & $V$ & $V$ & $F$ & $F$ & $F$ & $F$ & $F$ & $F$\\
$(P_1 \wedge P_2) \vee (\lnot P_1 \wedge \lnot P_2)$ & $V$ & $V$ & $F$ & $F$ & $F$ & $F$ & $V$ & $V$\\
\eqref{eq:police1} & $V$ & $V$ & $V$ & $V$ & $F$ & $F$ & $V$ & $V$\\
\eqref{eq:police2} & $V$ & $V$ & $F$ & $F$ & $V$ & $V$ & $V$ & $V$\\
\eqref{eq:police3} & $V$ & $F$ & $V$ & $V$ & $V$ & $V$ & $V$ & $F$\\
\eqref{eq:police4} & $F$ & $V$ & $V$ & $V$ & $V$ & $V$ & $V$ & $F$\\
\end{tabular}};

%i Know, it's ugly as fuck here be I can't figure out how to do it with relative coordinates of (table)
\draw [red,ultra thick,rounded corners] (3.925,-0.45) rectangle (4.5,-2.3);
\draw [red,ultra thick,rounded corners] (3.925,2.4) rectangle (4.5,1);
\end{tikzpicture}
\caption{Table de vérité des propositions \eqref{eq:police1} à \eqref{eq:police4}}
\label{tab:police}
\end{center}
\end{table}

On remarque sur cette table que la seule possibilité pour que les propositions \eqref{eq:police1} à \eqref{eq:police4} soient vraies en même temps est que $P_1$ et $P_2$ soient vraies, et que $P_3$ soit faux. Ces cas de figure sont entourés en rouge dans la table de vérité.

L'inspecteur doit donc interroger Alex ou Virginie à propos du meurtre, car il est certain qu'ils disent la vérité !
\end{sol}

\begin{exo}
Mario tente (encore) de sauver la princesse Peach des griffes de l'infâme Bowser. Sachant son univers truffé de monstres et de passages secrets, il se dit alors « Pour chaque château de Bowser, il existe un passage secret que je peux emprunter afin d'atteindre Peach en toute sécurité ». Son frère Luigi est néanmoins pessimiste, et pense exactement le contraire. Que pense Luigi ?
\end{exo}

\begin{sol}
On modélise cette situation de la façon suivante. Posons $s(c,p)$ le prédicat « j'emprunte le passage $p$ dans le château $c$ » et $q$ la proposition « j'atteins Peach en toute sécurité~».

La proposition logique que suit Mario est donc 
\begin{equation*}
\forall c, \exists p, s(c,p) \then q.
\end{equation*}
Luigi, pessimiste, pense le contraire de cette proposition, c'est-à-dire sa négation. Luigi pense donc 
\begin{align*}
& \exists c, \forall p, \lnot (s(c,p) \then q)\\
\ssi & \exists c, \forall p, s(c,p) \wedge \lnot q.
\end{align*}
En français, Luigi pense donc « il existe un château tel que pour tout passage secret que j'emprunte, je n'atteins pas Peach en toute sécurité », ou encore «~il existe un château sans passage secret permettant d'atteindre Peach en toute sécurité ».
\end{sol}