\chapter{Suites et séries}\label{chap:sequences}

\begin{exo}
Déterminez les six premiers termes des suites suivantes, décrites récursivement :
\begin{align*}
x_n &= \left\{
	\begin{array}{ll}
	3 & \textrm{ si } n = 0,\\
	x_{n-1} + 2 & \textrm{ sinon},
	\end{array}
	\right.\\	
y_n &= \left\{
	\begin{array}{ll}
	1 & \textrm{ si } n = 0,\\
	4y_{n-1} & \textrm{ sinon},
	\end{array}
	\right.\\		
z_n &= \left\{ %n^2 + n
	\begin{array}{ll}
	0 & \textrm{ si } n = 0,\\
	z_{n-1} + 2n & \textrm{ sinon}.
	\end{array}
	\right.\\	
\end{align*}
Pour chacune d'elles, exprimez également le terme général sous forme non-récursive.
\end{exo}

\begin{sol}
Les six premiers éléments des suites demandées sont les suivants.
\begin{enumerate}
\item 3, 5, 7, 9, 11, 13;
\item 1, 4, 16, 64, 256, 1024;
\item 0, 2, 6, 12, 20, 30. \\
\end{enumerate}
De plus, on a 
\begin{enumerate}
\item $x_n = 2n + 3$,
\item $y_n = 4^{n}$,
\item $z_n = n^2 + n$.\\
\end{enumerate}
Notez que pour être entièrement formel, il est nécessaire de prouver que les formules avancées correspondent bien à la relation de récurrence définissant le terme général des suites ci-dessus. Classiquement, on applique une \emph{preuve par récurrence} afin de prouver un tel résultat.
\end{sol}

\begin{exo}
Définissez les suites suivantes par récurrence :
\begin{itemize}
\item $(x_n) = n + 3$,
\item $(y_n) = 2^n + 2$, % 3 4 6 10 18
\item $(z_n) = 2n(n+1)$. %0 4 12 24 60
\end{itemize}
\end{exo}

\begin{sol}
On définit par récurrence ces suites de la façon suivante.
\begin{align*}
x_n &= \left\{
	\begin{array}{ll}
	3 & \textrm{ si } n = 0,\\
	x_{n-1} + 1 & \textrm{ sinon},
	\end{array}
	\right.\\	
y_n &= \left\{
	\begin{array}{ll}
	3 & \textrm{ si } n = 0,\\
	2y_{n-1} - 2 & \textrm{ sinon},
	\end{array}
	\right.\\		
z_n &= \left\{ 
	\begin{array}{ll}
	0 & \textrm{ si } n = 0,\\
	z_{n-1} + 4n & \textrm{ sinon}.
	\end{array}
	\right.\\	
\end{align*}
Encore une fois, pour être entièrement formel, il est nécessaire de prouver que les formules avancées correspondent bien à la relation de récurrence définissant le terme général des suites ci-dessus.
\end{sol}

\begin{exo}
Déterminez si les suites suivantes sont croissantes, décroissantes, bornées ou non bornées.
\begin{itemize}
\item $(x_n) = e^{-n}$,
\item $(y_n) = n^3$,
\item $(z_n) = n \cdot (-1)^n$.
\end{itemize}
\end{exo}

\begin{sol}
Les suites suivantes possèdent les caractéristiques suivantes.
\begin{itemize}
\item $e^{-n}$ est décroissante. En effet, 
	\begin{align*}
	\forall n,\quad 	 & e^{-n} > e^{-(n + 1)} \\
			   \ssi\quad & e^{-n} > e^{- n - 1} \\
			   \ssi\quad & -n > - n - 1& \textrm{par utilisation de la fonction $\ln$, croissante}\\
			   \ssi\quad & 0 > -1\\
	\end{align*}
	De plus, cette fonction est bornée supérieurement par $1$ (car $e^{0} = 1$ et $e^{-n}$ est décroissante).
	Elle est également bornée inférieurement par $0$, car $e^{-n} > 0$ quel que soit $n$.\\
	
\item $n^3$ est croissante. En effet,
	\begin{align*}
	\forall n,\quad 	 & n^3 < (n + 1)^3 \\
			   \ssi\quad & n^3 < n^3 + 3n^2 + 3n + 1 \\
			   \ssi\quad & 0 < 3n^2 + 3n + 1\\
	\end{align*}
	ce qui est toujours vrai car $n \geqslant 0$. De plus, cette fonction est bornée inférieurement par $1$, car $n^0 = 1$ et $n^3$ est croissante (le premier élément de cette suite est égal à un, et tous les autres sont supérieurs). 
	Cette suite n'est par contre pas bornée supérieurement, car $\displaystyle \lim_{n \rightarrow +\infty} n^3 = +\infty$.\\
	
\item Cette suite ni croissante, ni décroissante, car elle est alternée. En effet,
	\begin{align*}
	\forall n,\quad 	 & n(-1)^n \cdot (n+1)(-1)^{n+1} < 0 \\
			   \ssi\quad & n(n+1)(-1)^{2n+1} < 0 \\
	\end{align*}
	ce qui est toujours vrai car $n \geqslant 0$ et $2n+1$ est impair (donc $(-1)^{2n+1} = -1$).
	Cette suite n'est bornée ni supérieurement, ni inférieurement, car elle contient les sous-suites $n(-1)^{2n}$ et $n(-1)^{2n+1}$ qui ne sont pas bornées.
\end{itemize}
\end{sol}

\begin{exo}
Considérez que les suites $(x_n)$ de cet exercice sont arithmétiques.
\begin{enumerate}
    \item Si $x_0 = 3$ et $r = 7$, que vaut $x_{4}$ ?
    \item Si $x_3 = 17$ et $x_{12} = -5.5$, que vaut $r$ ?
    \item Si $x_{2016} = 36$ et $r = 3$, que vaut $x_5$ ?
\end{enumerate}
\end{exo}

\begin{sol}
Pour répondre à la question demandée, on utilise la propriété \ref{prop:arithm}.
\begin{enumerate}
\item On a $x_4 = x_0 + (4 - 0) \cdot r = 3 + 4 \cdot 7 = 31$.
\item On a $r = \displaystyle \frac{x_{12} - x_3}{12 - 3} = \displaystyle \frac{-5.5 - 17}{12 - 3} = \displaystyle \frac{-22.5}{9} = -\frac{5}{2}\cdot$
\item On a $x_5 = x_{2016} - (2016 - 5) \cdot r = 36 - 2011 \cdot 3 = 36 - 6033 = -5997$.
\end{enumerate}
\end{sol}

\begin{exo}
Considérez que les suites de cet exercice sont géométriques.
    \begin{enumerate}
    \item Si $u_0 = 2$ et $q = \frac{2}{3}$, que vaut $u_7$ ?
    \item Si $u_3 = 20$ et $u_{6} = 540$, que vaut $q$ ?
    \item Si $u_8 = 3$ et $q = -\frac{1}{2}$, que vaut $u_1$ ?
    \end{enumerate}
\end{exo}

\begin{sol}
Pour répondre à la question demandée, on utilise la propriété \ref{prop:geom}.
\begin{enumerate}
\item On a $u_7 = u_0 \cdot q^{7 - 0} = 2 \cdot \left(\frac{2}{3}\right)^{7} = 2 \cdot \frac{128}{2187} = \frac{256}{2187}$.
\item On a $q = \sqrt[6-3]{\frac{540}{20}} = \sqrt[3]{27} = 3$.
\item On a $u_1 = u_8 \cdot q^{1 - 8} = 3 \cdot \left( -\frac{1}{2}\right)^{-7}  = 3 \cdot 128 = -384$.
\end{enumerate}
\end{sol}

\begin{exo}
Caractérisez la convergence des suites suivantes.
\begin{itemize}
\item $w_n = \displaystyle \frac{1}{n^2} + 2$.
\item $x_n = e^{-n^2}$,
\item $y_n = 3n^3+2n^2+n-1$,
\item $z_n = \displaystyle \sin\left(\frac{n\pi}{2}\right)$.
\end{itemize}
\end{exo}

\begin{sol}
On utilise principalement le théorème du sandwich pour répondre à cette question.
\begin{itemize}
\item On a, quel que soit $n \geqslant 0$, que $2 < \frac{1}{n^2} + 2 < \frac{1}{n} + 2$. Or, comme $\frac{1}{n} + 2 \rightarrow 2$ (car $\frac{1}{n} \rightarrow 0$), on a $\frac{1}{n^2} + 2 \rightarrow 2$.
\item On a, quel que soit $n \geqslant 0$, que $0 < e^{-n^2} < e^{-n}$. Or, comme $e^{-n} \rightarrow 0$, on a $e^{-n^2} \rightarrow 0$.
\item La suite $y_n$ ne converge pas, car elle est non bornée. En effet, pour tout $M$ pour tout $n_0$, il existe $n>n_0$ tel que on a $3n^3+2n^2+n-1 > M$. On remarque également que $\displaystyle \lim_{n \rightarrow +\infty} y_n = +\infty$.
\item On remarque que cette suite est en réalité $0, 1, 0, -1, 0, 1, 0, -1,\dots$ (par définition de la fonction sinus). Cette suite oscille entre quatre valeurs, elle ne converge donc pas.
\end{itemize}
\end{sol}

\begin{exo}
Montrez que la suite $(e^{-n})$ converge vers zéro, en utilisant la définition de convergence.
\end{exo}

\begin{sol}
On doit montrer que $\displaystyle \lim_{n \rightarrow \infty} e^{-n} = 0$, c'est à dire montrer que
\begin{equation*}
\forall \varepsilon > 0, \exists n_0 \in \IN, \forall n \geqslant n_0, |x_n| < \varepsilon.
\end{equation*}

On va donc prendre $\varepsilon$ arbitraire et trouver un $n_0$, probablement fonction de $\varepsilon$, tel que $n \geqslant n_0 \then |e^{-n}| < \varepsilon$, ou encore $e^{-n} < \varepsilon$, car $e^{-n} > 0$ quel que soit $n$.

Soit $\varepsilon > 0$, posons $n_0 = \ceil{-\mathrm{ln}\,\varepsilon}$. Si $n \geqslant n_0$, on a $n \geqslant \ceil{-\mathrm{ln}\,\varepsilon}$, par définition de $n_0$. Ceci entraîne que
\begin{equation*}
\begin{array}{cll}
	 & n \geqslant -\mathrm{ln}\,\varepsilon & \textrm{ par définition de } \ceil{\cdot}\\
\ssi & -n \leqslant \mathrm{ln}\,\varepsilon & \\
\ssi & e^{-n} \leqslant \varepsilon & \textrm{ car } e^n \textrm{ est croissant sur } [0,+\infty[ \textrm{ car } n \geqslant 0\\
\end{array}
\end{equation*}
\end{sol} 

\begin{remark}
Pour rappel, l'utilisation de cette définition ne fait pas partie des compétences à acquérir pour le cours, et n'est illustrée ici qu'à titre d'exemple.
\end{remark}

\begin{exo}
Analysez la convergence des séries suivantes :
\begin{enumerate}
\item $\displaystyle \sum_{i=1}^{\infty} \frac{\sin(n) + 2}{n}$,
\item $\displaystyle \sum_{i=0}^{\infty} \frac{1}{n^3 + 2n^2 + 4n + 1}$,
\item $\displaystyle \sum_{i=0}^{\infty} (-1)^n \cdot (n \DIV 2)$.
\end{enumerate}
\end{exo}

\begin{sol}
On caractérise la convergence de ces séries de la façon suivante.
\begin{enumerate}
\item Avant toute chose, notons que le terme général de la suite sous-jacente converge vers zéro. Cette suite est donc éligible pour la convergence, par la propriété \ref{prop:serie-nec}.

Néanmoins, comme $-1 \leqslant \sin(n) \leqslant 1$, on a $1 \leqslant sin(n) + 2 \leqslant 3$, et donc 
\begin{equation*}
\displaystyle \sum_{i=1}^{\infty} \frac{\sin(n) + 2}{n} \geqslant \sum_{i=1}^{\infty} \frac{1}{n}\cdot
\end{equation*}

Comme la série harmonique diverge, cette suite diverge également.

\item Encore une fois, le terme général de la suite sous-jacente converge vers zéro. Cette suite est donc éligible pour la convergence, par la propriété \ref{prop:serie-nec}.

De plus, on remarque que 
\begin{equation*}
\sum_{i=0}^{\infty} \frac{1}{n^3 + 2n^2 + 4n + 1} < \sum_{i=0}^{\infty} \frac{1}{n^3}\cdot
\end{equation*}
Or, $\displaystyle \sum_{i=0}^{\infty} \frac{1}{n^3}$ est une p-série avec $p > 1$. Elle converge donc. Ainsi, par corollaire du théorème du sandwich, la série originale converge également.

\item On remarque que le terme général $(-1)^n \cdot (n \DIV 2)$ de la suite sous-jacente ne converge pas vers zéro. La série ne peut donc pas converger, par la propriété \ref{prop:serie-nec}.
\end{enumerate}
\end{sol}