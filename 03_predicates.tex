\chapter{Logique des prédicats}\label{chap:preds}

\begin{detail}
La logique des prédicats vient compléter l'algèbre booléenne dans la mise en œuvre théorique de la logique mathématique. Intuitivement, un prédicat est une proposition qui comporte des variables. Les prédicats apportent de la puissance aux formules booléennes, dans le sens où l'introduction de variables permet d'exprimer plus de choses. On peut par exemple écrire des propositions telles que «~tous les chats sont gris~».

Plus particulièrement, la section~\ref{sec:pred-basis} introduit les concepts élémentaires à la compréhension de prédicat. Ensuite, la section~\ref{sec:set-pred} revient sur les similarités entre connecteurs logiques et ensemblistes, et leur impact sur les prédicats. %Finalement, la section~\ref{sec:quant} introduit le concept de \emph{quantificateurs}, indispensables à la manipulation de prédicats.

Comme d'habitude, ce chapitre est conclut par une série d'exercices résolus en section~\ref{sec:pred-exos-res}.
\end{detail}

\section{Concepts de base}\label{sec:pred-basis}

Une variable est simplement un symbole dont la valeur, \emph{a priori} inconnue, est comprise dans un certain ensemble de valeurs possibles. On peut par exemple restreindre une variable à des valeurs réelles, entières, booléennes, etc.

\begin{defbox}{}{}
Un \emph{prédicat}, une \emph{fonction propositionnelle} ou une \emph{condition} est une expression dont la valeur de vérité (vraie ou fausse) dépend d'une ou plusieurs variables.\indext{Prédicat}
\end{defbox}

\begin{example}
Les deux lignes suivantes illustrent des exemples de prédicats, l'un à une variable $x$, et l'autre à deux variables $x$ et $y$. 
\begin{align}
p(x) :~& x \textrm{ est rouge},\label{eqt:red}\\
q(x,y) :~& x \textrm{ est jaune et } y \textrm{ a un noyau}.\label{eqt:redcore}
\end{align}
\end{example}

Évidemment, construire la table de vérité de tels prédicats peut s'avérer difficile : il faut considérer toutes les valeurs possibles des variables, ce qui peut être fastidieux. En réalité, souvent, une telle énumération n'est pas possible, à cause du nombre de valeurs différentes que peuvent prendre ces variables. Pour cette raison, on utilise un concept similaire aux tables de vérité : les \emph{classes de vérité}.

Afin de clairement définir un prédicat, il faut restreindre les valeurs possibles de ses variables. L'ensemble des valeurs que peuvent prendre ces variables est appelé le \emph{domaine de définition} du prédicat. L'ensemble des valeurs du domaine de définition qui donnent la valeur « vrai » au prédicat est appelé la \emph{classe de vérité} du prédicat. 

Plus formellement, on définit ces deux concepts de la façon suivante.

\begin{defbox}{}{}
Soit $P$ un prédicat, le \emph{domaine de définition} de $P$, noté $\Dom{P}$, est l'ensemble sur lequel le prédicat peut être défini. \indexnt{dom}{$\Dom{P}$}{Domaine de définition}
\end{defbox}

Pour simplifier, on considérera toujours ce domaine comme non-vide.

\begin{defbox}{}{}
Soit $P$ un prédicat, la classe de vérité de $P$, notée $\cv{P}$, est l'ensemble des éléments du domaine de définition qui vérifient $P$.\indexnt{cv}{$\cv{P}$}{Classe de vérité}
\end{defbox}

\begin{example}
Considérez le prédicat «~$x$ est rouge~», noté $p(x)$. Posons son domaine de définition comme l'ensemble des fruits. Cela signifie que le prédicat n'a de sens uniquement que lorsqu'il est évalué sur un fruit : $p(pomme)$ et $p(banane)$ ont un sens mathématique, $p(coquelicot)$ n'a pas de sens au vu de ce domaine de définition.

«~Framboise~» fait partie de la classe de vérité de ce prédicat. C'est en effet un fruit, qui est rouge. La classe de vérité $\cv{P}$ est en fait l'ensemble des fruits rouges. Ce prédicat n'est évidemment pas une tautologie : certains fruits ne font pas partie de son domaine de définition. Par exemple, « banane » ne fait pas partie de la classe de vérité de ce prédicat, car une banane n'est pas rouge. Par ailleurs, se demander si «~coquelicot~» fait partie de la classe de vérité de $P$ n'a pas de sens, car « coquelicot » ne fait pas partir du domaine de définition de~$P$.
\end{example}

\begin{example}
Soient $p(x)$ et $q(x)$ deux prédicats définis %\footnote{Bien qu'il puisse sembler étrange de placer un opérateur $=$ à cet endroit dans les définitions de $p(x)$ et $q(x)$, cette syntaxe est néanmoins tout à fait exacte : $p(x)$ est un prédicat qui est égal à l'expression booléenne $2 \leqslant x < 7$ qui suit.}
comme
\begin{align*}
p(x) &: 2 \leqslant x < 7,\\
q(x) &: 5 < x < 9.\\
\end{align*}

Cherchons la classe de vérité de $p(x) \vee q(x)$ en procédant de la manière suivante :
\begin{itemize}
\item on établit une table de vérité à partir d'une analyse de tout intervalle où les valeurs de vérité de $p(x)$ et $q(x)$ ne varient pas;
\item on dessine une droite et on y place les classes de vérité.\\
\end{itemize}

Dans la suite de cet exemple, on va considérer deux cas d'énoncé différents. Le premier, où le domaine de définition de $p(x)$ et de $q(x)$ sera $\IR$, et le deuxième uniquement $\IZ$.

\textsc{Cas 1} : considérons $\IR$ comme le domaine de définition de $p(x)$ et $q(x)$. Ainsi, aussi bien $2$ que $2,5$ appartiennent au domaine de définition de définition de ces prédicats.

La classe de vérité de $p(x) \vee q(x)$ dans ce cas peut être construite comme à la figure \ref{fig:cvpqr}. En résumé, cette classe de vérité est l'intervalle des nombres compris entre $2$ (inclusivement) et $9$ (exclusivement).

\begin{figure}[!htp]
\begin{center}
\begin{tikzpicture}[scale=.9]
	% axe
	\draw[->] (-.5,3) -- (11,3) node[right, scale=1.5] {$\mathbb{R}$};
	% \draw (11,3) node[right,scale=1.5] {$\mathbb{R}$};
	%\node [right,scale=1.5] at (11,3) {$\mathbb{R}$};
	\foreach \x in {0,1,...,10} {
		% \draw (\x,3) node [below] {\x};
		\node [below] at (\x,3) {\x};
		\draw (\x,3.1) -- (\x,2.9);
	}
	% p(x)
	\node (minp) [circle,draw,color=black,fill=black] at (2,2) {};
	\node (maxp) [circle,draw,color=black] at (7,2) {};
	\draw [dotted] (-.5,2) -- (minp);
	\draw [line width=2pt] (minp) -- (maxp);
	\draw [dotted] (maxp) -- (10.5,2);
	\node [right,scale=.8] at (8,1.6) {classe de vérité de $p(x)$};
	% q(x)
	\node (minq) [circle,draw,color=black] at (5,1) {};
	\node (maxq) [circle,draw,color=black] at (9,1) {};
	\draw [dotted] (-.5,1) -- (minq);
	\draw [line width=2pt] (minq) -- (maxq);
	\draw [dotted] (maxq) -- (10.5,1);
	\node [right,scale=.8] at (8,.6) {classe de vérité de $q(x)$};
	% p(x) v q(x)
	\node (minpvq) [circle,draw,color=black,fill=black] at (2,0) {};
	\node (maxpvq) [circle,draw,color=black] at (9,0) {};
	\draw [dotted] (-.5,0) -- (minpvq);
	\draw [line width=2pt] (minpvq) -- (maxpvq);
	\draw [dotted] (maxpvq) -- (10.5,0);
	\node [right,scale=.8] at (8,-0.4) {classe de vérité de $p(x) \vee q(x)$};
\end{tikzpicture}\vspace{0.5cm}
\begin{tabular}{cccc}
\hline
$x$ & $p(x)$ & $q(x)$ & $p(x) \vee q(x)$\\
\hline
$] -\infty, 2 [$ & $F$ & $F$ & $F$\\
$[ 2, 5 ]$ & $V$ & $F$ & $V$\\
$] 5, 7 [$ & $V$ & $V$ & $V$\\
$[ 7, 9 [$ & $F$ & $V$ & $V$\\
$[ 9, +\infty [$ & $F$ & $F$ & $F$\\
\hline
\end{tabular}
\caption{Illustration de la classe de vérité de $p(x) \vee q(x)$ (cas 1)}
\label{fig:cvpqr}
\end{center}
\end{figure}

\begin{remark}
Sur la figure \ref{fig:cvpqr}, un gros point noir signifie que l'on prend l'élément considéré dans la classe de vérité, alors qu'un point blanc encerclé de noir signifie que l'on ne prend pas l'élément en question. Cette convention de représentation sera utilisée tout au long de ce document lors d'illustrations de concepts.
\end{remark}

\textsc{Cas 2} : considérons à présent uniquement $\IN$ comme le domaine de définition de ces prédicats. Ainsi, $2$ appartient au domaine de définition de ces prédicats, mais pas $2,5$ ni $-1$.

La classe de vérité de $p(x) \vee q(x)$ peut être dans ce cas illustrée comme à la figure \ref{fig:cvpqn}. Notez que cette fois-ci, on ne peut plus utiliser de droite «~continue~», on a donc opté pour de simples points.

En résumé, la classe de vérité de $p(x) \vee q(x)$ est les nombres entiers allant de $2$ à $8$.

\begin{figure}[!htp]
\begin{center}
\newcommand{\cvpx}{
plot [variable=\t,samples=100,domain=0:2*pi]%
({4+2.5*sin(deg(\t))},{1+1*cos(deg(\t))})
(3,2.3) node [scale=1] {classe de vérité de $p(x)$}
}

\newcommand{\cvqx}{
plot [variable=\t,samples=100,domain=0:2*pi]%
({7+1.5*sin(deg(\t))},{1+1*cos(deg(\t))})
(8.5,2.3) node [scale=1] {classe de vérité de $q(x)$}
}

\begin{tikzpicture}[scale=1]
	% axe naturel
	\draw [loosely dotted] (10.4,3.5) -- (10.9,3.5);
	\node [right,scale=1.5] at (11,3.5) {$\mathbb{N}$};
	\foreach \x in {0,1,...,10} {
		\node at (\x,3.1) {\x};
		\draw (\x,3.5) circle (2pt);
	}
	% classes de vérité
	\fill [lightgray] \cvpx;
	\fill [lightgray] \cvqx;
	\draw [loosely dotted] (10.4,1) -- (10.9,1);
	\node [right,scale=1.5] at (11,1) {$\mathbb{N}$};
	\foreach \x in {0,1,...,10} {
		\node at (\x,.6) {\x};
		\draw (\x,1) circle (2pt);
	}
	\draw \cvpx;
	\draw \cvqx;
	\node at (5,-.3) {classe de vérité de $p(x) \vee q(x)$};
\end{tikzpicture}
\caption{Illustration de la classe de vérité de $p(x) \vee q(x)$ (cas 2)}
\label{fig:cvpqn}
\end{center}
\end{figure}
\end{example}

\begin{detail}
\begin{example}
Soit le prédicat $p(x,y)$ défini comme «~l'étudiant $x$ a le cours $y$ dans son programme~». Si on considère $E$ comme l'ensemble des étudiants et $C$ comme l'ensemble des cours dispensés à l'école, le domaine de définition de $p$ est $E \times S$. La classe de vérité de $p$ est l'ensemble des couples $(x,y)$ tels que l'étudiant $x$ a le cours $y$ dans son programme.
\end{example}
\end{detail}

\section{Connexion entre prédicats et opérateurs ensemblistes}\label{sec:set-pred}

On a déjà vu aux chapitres \ref{chap:bool} et \ref{chap:sets} que les connecteurs logiques et opérateurs ensemblistes sont très semblables de par leur fonctionnement. Du point de vue des classes de vérité de prédicats, ces similarités sont toujours très présentes. Cette section détaille ces similitudes à l'aide de diagrammes de Venn.

Soient $p(x)$ et $q(x)$ des prédicats dont le domaine de définition est $S$, on peut les connecter entre eux et la valeur de vérité de du résultat, également un prédicat, est obtenue en utilisant les tables de vérités.

Pour chacun des opérateurs logiques présentés, on a donc les propriétés suivantes.
\begin{description}
	\item[Négation] \hfill \\
%		\[
%			\mcv{\mnot p(x)} = \complement_{E}\Bigl(\mcv{p(x)}\Bigr)
%		\]
\newcommand{\vennE}{
(-6,-1.4) rectangle (6,6)
(5,6) node [above,scale=1] {$S$}
}
%
\newcommand{\vennCVp}{
plot [variable=\t,samples=100,domain=0:2*pi]%
({3*sin(deg(\t))},{3+2*cos(deg(\t))})
(-3,-1.5) node [above,scale=1] {$\cv{p(x)}$}
}

\begin{tikzpicture}[scale=.3]
    \draw (-16,3) node{$\displaystyle \cv{\lnot p(x)} = \complement_{S}\Bigl(\cv{p(x)}\Bigr)$};
	\filldraw[draw=black,fill=lightgray,line width=.5mm] \vennE; 
	\filldraw[draw=black,fill=white,line width=.5mm] \vennCVp;
\end{tikzpicture}

En d'autres termes, la classe de vérité de la négation d'un prédicat $p$ est égale au complémentaire de la classe de vérité de $p$.
%
	\item[Conjonction] \hfill \\
%		\[
%			\mcv{p(x) \mand q(x)} = \mcv{p(x)} \cap \mcv{q(x)}
%		\]
\renewcommand{\vennE}{
(-8.5,-1.5) rectangle (8.5,6)
(7.5,6) node [above,scale=1] {$S$}
}
%
\newcommand{\vennCVpSansLabel}{
plot [variable=\t,samples=100,domain=0:2*pi]%
({-2+3*sin(deg(\t))},{3+2*cos(deg(\t))})
}

\newcommand{\vennCVqSansLabel}{
plot [variable=\t,samples=100,domain=0:2*pi]%
({2+3*sin(deg(\t))},{3+2*cos(deg(\t))})
}
%
\renewcommand{\vennCVp}{
%plot [variable=\t,samples=100,domain=0:2*pi]%
%({-2+3*sin(deg(\t))},{3+2*cos(deg(\t))})
\vennCVpSansLabel
(-5.3,-1.5) node [above,scale=1] {$\cv{p(x)}$}
}
%
\newcommand{\vennCVq}{
%plot [variable=\t,samples=100,domain=0:2*pi]%
%({2+3*sin(deg(\t))},{3+2*cos(deg(\t))})
\vennCVqSansLabel
(5.5,-1.5) node [above,scale=1] {$\cv{q(x)}$}
}
%
\begin{tikzpicture}[scale=.3]
    \draw (-21,3) node{$\displaystyle \cv{p(x) \wedge q(x)} = \cv{p(x)} \cap \cv{q(x)}$};
	\draw [draw=black,line width=.5mm] \vennE;
	\begin{scope}
		\clip \vennCVq;
		\fill [fill=lightgray] \vennCVp;
	\end{scope}
	\draw [draw=black,line width=.5mm] \vennCVp;
	\draw [draw=black,line width=.5mm] \vennCVq;
\end{tikzpicture}

Autrement dit, la classe de vérité de la conjonction de deux prédicats est égale à l'intersection des classes de vérité de ces prédicats.

	\item[Disjonction] \hfill \\
%		\[
%			\mcv{p(x) \mor q(x)} = \mcv{p(x)} \cup \mcv{q(x)}
%		\]
\begin{tikzpicture}[scale=.3]
    \draw (-21,3) node{$\displaystyle \cv{p(x) \vee q(x)} = \cv{p(x)} \cup \cv{q(x)}$};
	\draw [draw=black,line width=.5mm] \vennE; 
	\filldraw [draw=black,fill=lightgray,line width=.5mm] \vennCVp;
	\filldraw [draw=black,fill=lightgray,line width=.5mm] \vennCVq;
	\begin{scope}
		\clip \vennCVq;
		\draw [draw=black,line width=.5mm] \vennCVp;
	\end{scope}
\end{tikzpicture}

Ceci signifie que la classe de vérité de la disjonction de deux prédicats est égale à l'union des classes de vérité de ces prédicats.
%\pagebreak
	\item[Disjonction exclusive] \hfill \\
%		\[
%			\mcv{p(x) \mxor q(x)} = \mcv{p(x)} \bigtriangleup \mcv{q(x)}
%		\]\begin{center
\begin{tikzpicture}[scale=.3]
    \draw (-21,3) node{$\displaystyle \cv{p(x) \veebar q(x)} = \cv{p(x)} \bigtriangleup \cv{q(x)}$};
	\draw [draw=black,line width=.5mm] \vennE;
	\fill [fill=lightgray] \vennCVpSansLabel;
	\fill [fill=lightgray] \vennCVqSansLabel;
	\begin{scope}
		\clip \vennCVq;
		\fill [fill=white] \vennCVp;
	\end{scope}
	\draw [draw=black,line width=.5mm] \vennCVp;
	\draw [draw=black,line width=.5mm] \vennCVq;
\end{tikzpicture}

En d'autres termes, la classe de vérité de la disjonction exclusive de deux prédicats est la différence symétrique entre les classes de vérité de ces prédicats.

	\item[Implication] \hfill \\
%		\[
%			\mcv{p(x) \mimp q(x)} = 
%	    		\complement_{E}\Bigl(\mcv{p(x)}\Bigr) \cup \mcv{q(x)}
%	    \]
\hspace*{-1cm}\begin{tikzpicture}[scale=.3]
    \draw (-22.5,3) node{$\displaystyle \cv{p(x) \then q(x)} = \complement_{S}\Bigl(\cv{p(x)}\Bigr) \cup \cv{q(x)}$};
	\filldraw [draw=black,fill=lightgray,line width=.5mm] \vennE;
	\filldraw[draw=black,fill=white,line width=.5mm] \vennCVp;
	\filldraw [draw=black,fill=lightgray,line width=.5mm] \vennCVq;
	\begin{scope}
		\clip \vennCVq;
		\draw [draw=black,line width=.5mm] \vennCVp;
	\end{scope}
\end{tikzpicture}

Ceci signifie que la classe de vérité de l'implication d'un prédicat $q$ par un prédicat $p$ est égale à l'union entre le complémentaire de la classe de vérité de $p$ et la classe de vérité de $q$. Ceci peut-être facilement déduit des relations précédentes via la définition positive de l'implication implication.

	\item[Équivalence] \hfill \\
%		\[
%			\mcv{p(x) \mequ q(x)} = 
%	    		\complement_{E}\Bigl(\mcv{p(x)} \bigtriangleup 
%	    		                        \mcv{q(x)}\Bigr)
%	    \]
\hspace*{-1cm}
\begin{tikzpicture}[scale=.3]
    \draw (-22.5,3) node{$\displaystyle \cv{p(x) \ssi q(x)} = \complement_{S}\Bigl(\cv{p(x)} \bigtriangleup \cv{q(x)}\Bigr)$};
	\filldraw [draw=black,fill=lightgray,line width=.5mm] \vennE;
	\filldraw[draw=black,fill=white,line width=.5mm] \vennCVp;
	\filldraw [draw=black,fill=white,line width=.5mm] \vennCVq;
	\begin{scope}
		\clip \vennCVq;
		\filldraw [draw=black,fill=lightgray,line width=.5mm] \vennCVp;
	\end{scope}
	\begin{scope}
		\clip \vennCVp;
		\draw [draw=black,line width=.5mm] \vennCVq;
	\end{scope}	
\end{tikzpicture}

Enfin, la classe de vérité de deux prédicats équivalents peut être construite comme le complémentaire de la différence symétrique entre les classes de vérité des deux prédicats. Ceci est normal, étant donné que deux prédicats sont équivalents s'ils ont la même valeur de vérité, et que la différence symétrique définit les éléments qui sont soit dans un ensemble, soit dans un autre.
%
\end{description}

\begin{detail}
\section{Exercices résolus}\label{sec:pred-exos-res}

\begin{exo}
Soit $p(x)$ le prédicat « le mot $x$ contient la lettre 'a' », défini sur l'ensemble des mots de la langue française. Quelles sont les valeurs de vérité de
\begin{itemize}
\item $p(\textrm{orange})$,
\item $p(\textrm{citron})$,
\item $p(\textrm{vrai})$,
\item $p(\textrm{faux})$.
\end{itemize}
\end{exo}

\begin{sol}
Ces propositions ont toutes la valeur de vérité vrai, à l'exception de $p(\textrm{citron})$, car « citron » ne contient pas la lettre $a$.
\end{sol}

\begin{exo}
Soit $p(x)$ le prédicat $x > 5$. Donnez la valeur de $x$ après qu'une instruction\footnote{Dans cette instruction, le symbole « $:=$ » dénote l'affectation d'une variable à une valeur.} de type « si $p(x)$, alors $x := 1$ », si la valeur de $x$ quand cette instruction est atteinte est
\begin{itemize}
\item $x=0$,
\item $x=1$,
\item $x=6$.
\end{itemize}
\end{exo}

\begin{sol}
L'implication détermine ici la valeur de $x$. Si la valeur de vérité du prédicat évalué est vraie, la valeur de $x$ est changée à $1$, sinon, elle reste à sa valeur originale. On a donc, après exécution :
\begin{itemize}
\item $x = 0$,
\item $x = 1$,
\item $x = 1$.
\end{itemize}
\end{sol}

\begin{exo}
Soit $A=\Set{n \in \IN \suchthat n \mod 3 = 2 \wedge n < 20}$. Déterminez la classe de vérité de 
\begin{equation*}
p(x) = (x + 2)^2 \mod 3 = 0,
\end{equation*}
avec $x \in A$.
\end{exo}

\begin{sol}
Avant toutes choses, on remarque que $A = \Set{2, 5, 8, 11, 14, 17}$. Ensuite, il n'existe aucun élément $x$ de cet ensemble tel que $(x+2)^2$ est divisible par $3$. En conséquence, $\cv{p(x)} = \emptyset$.
\end{sol}

\begin{exo}
Soit un groupe d'amis dont certaines caractéristiques sont reprises à la table~\ref{tab:friends}. 
\begin{itemize}
	\item Quelle est la classe de vérité du prédicat $c(x) : x$ aime le chocolat ?
	\item Traduisez en logique des prédicats et déterminer la classe de vérité de la phrase : «~Si un des membres du groupe aime le chocolat alors il aime les gâteaux mais pas les biscuits~». 
\end{itemize}

\begin{table}[!htp]
\begin{center}
\begin{tabular}{|c|c|c|c|}
\hline
      & \textbf{Aime le chocolat ?} & \textbf{Aime les gâteaux ?} & \textbf{Aime les biscuits ?}\\
\hline
Abdel & vrai & faux & vrai \\
\hline
Gabriella & faux & faux & vrai \\
\hline
Paul & vrai & vrai & vrai \\
\hline
Fatima & faux & faux & faux \\
\hline
Merveille & vrai & vrai & faux\\
\hline
\end{tabular}
\caption{Préférences d'un groupe d'amis}
\label{tab:friends}
\end{center}
\end{table}
\end{exo}

\begin{sol}
On suppose pour cet exercice que les domaines de définitions des prédicats que l'on va utiliser est l'ensemble des $5$ personnes reprises à la table~\ref{tab:friends}.
\begin{itemize}
\item Le classe de vérité de $c(x)$ est $\Set{Abdel, Paul, Merveille}$.
\item En posant
	\begin{enumerate}
	\item $c(x) : x$ aime le chocolat,
	\item $g(x) : x$ aime le gâteau,
	\item $b(x) : x$ aime les biscuits,
	\end{enumerate}
	on peut exprimer cette phrase comme
\begin{equation*}
c(x) \then \big(g(x) \wedge \lnot b(x)\big).
\end{equation*}
Pour chacune des personnes, calculons la valeur de vérité du prédicat ci-dessus. Notez que dans le cas du calcul de la valeur de vérité d'une implication, on abrégera systématiquement le calcul de la valeur de vérité d'une implication dont la prémisse est fausse. En effet, cette valeur est toujours vraie.
	\begin{itemize}
	\item Abdel : 
		\begin{align*}
		     & c(Abdel) \then \big(g(Abdel) \wedge \lnot b(Abdel)\big) \\
		\ssi &  V \then \big( F \wedge F \big) \\
		\ssi & V \then F\\
		\ssi & F
		\end{align*}
	\item Gabriella : 
			\begin{align*}
			     & c(Gabriella) \then \big(g(Gabriella) \wedge \lnot b(Gabriella)\big) \\
			\ssi & F \then ... \\
			\ssi & V
			\end{align*}
	\item Paul : 
			\begin{align*}
			     & c(Paul) \then \big(g(Paul) \wedge \lnot b(Paul)\big) \\
			\ssi &  V \then \big( V \wedge F \big) \\
			\ssi & V \then F\\
			\ssi & F
			\end{align*}
	\item Fatima : 
				\begin{align*}
				     & c(Fatima) \then \big(g(Fatima) \wedge \lnot b(Fatima)\big) \\
				\ssi & F \then ... \\
				\ssi & V
				\end{align*}
	\item Merveille : 
				\begin{align*}
				     & c(Merveille) \then \big(g(Merveille) \wedge \lnot b(Merveille)\big) \\
				\ssi &  V \then \big( V \wedge V \big) \\
				\ssi & V \then V\\
				\ssi & V
				\end{align*}
	\end{itemize}
On peut donc conclure que la classe de vérité de ce prédicat est 
\begin{equation*}
\Set{Gabriella, Fatima, Merveille}.
\end{equation*}
\end{itemize}
\end{sol}

\begin{exo}
Soient $p(x)$ et $q(x)$ des prédicats définis sur l'ensemble des réels tels que
\begin{align*}
p(x) : & x < 3\\
q(x) : & x \geqslant 7,\\
\end{align*}
décrivez les classes de vérité de 
\begin{multicols}{3}
\begin{enumerate}
\item $p(x) \wedge q(x)$
\item $p(x) \vee q(x)$
\item $\lnot p(x) \wedge \lnot q(x)$
\item $\lnot p(x) \vee \lnot q(x)$
\item $p(x) \xor q(x)$
\item $p(x) \then q(x)$
\item $q(x) \then p(x)$
\item $p(x) \ssi q(x)$
\end{enumerate}
\end{multicols}
\end{exo}

\begin{sol}
Les classes de vérité des prédicats peuvent être construites et exprimées comme illustré à la figure~\ref{fig:exo-res-cv}. Notez que l'on a construit les classes de vérité de $\lnot p(x) \wedge \lnot q(x)$ et $\lnot p(x) \wedge \lnot q(x)$ par application des lois de De Morgan, en niant respectivement $p(x) \vee q(x)$ et $p(x) \wedge q(x)$.

\begin{figure}[!htp]
\begin{center}
\begin{tikzpicture}[xscale=.75]
	\draw[->] (-.5,0.5) -- (11,0.5) node[right, scale=1.5] {$\IR$};

	\foreach \x in {0,1,...,10} 
	{
		\node [below] at (\x,0.5) {\x};
		\draw (\x,0.6) -- (\x,0.4);
	}
	
	% p(x)		
	\draw [line width=2pt] (-.5,-1) -- (3,-1);
	\draw [dotted] (3,-1) -- (11,-1);
	\node [circle,draw,color=black,fill=white] at (3,-1) {};
	\node [anchor=west] at (9,-0.65) {$\cv{p(x)} = [-\infty, 3[$};
	
	%q(x)
	\node [circle,draw,color=black,fill=black] at (7,-2) {};
	\draw [line width=2pt] (7,-2) -- (11,-2);
	\draw [dotted] (-.5,-2) -- (7,-2);
	\node [anchor=west] at (9,-1.65) {$\cv{q(x)} = [7, +\infty[$};
	
	%p * q
	\draw [dotted] (-.5,-3) -- (11,-3);
	\node [anchor=west] at (9,-2.65) {$\cv{p(x) \wedge q(x)} = \emptyset$};
	
	%p + q	
	\node [circle,draw,color=black,fill=black] at (7,-4) {};
	\draw [line width=2pt] (-.5,-4) -- (3,-4);
	\draw [dotted] (3,-4) -- (7,-4);
	\draw [line width=2pt] (7,-4) -- (11,-4);
	\node [circle,draw,color=black,fill=white] at (3,-4) {};
	\node [anchor=west] at (9,-3.65) {$\cv{p(x) \vee q(x)} = [-\infty, 3[ \cup [7, +\infty[$};
	
	%-(p+q)
	\node [circle,draw,color=black,fill=black] at (3,-5) {};	
	\draw [dotted] (-.5,-5) -- (3,-5);
	\draw [line width=2pt] (3,-5) -- (7,-5);
	\draw [dotted] (7,-5) -- (11,-5);
	\node [circle,draw,color=black,fill=white] at (7,-5) {};
	\node [anchor=west] at (9,-4.65) {$\cv{\lnot p(x) \wedge \lnot q(x)} = [3, 7[$};
	
	%-(p * q)
	\draw [line width=2pt] (-.5,-6) -- (11,-6);
	\node [anchor=west] at (9,-5.65) {$\cv{\lnot p(x) \vee \lnot q(x)} = \IR$};

	%p xor q	
	\node [circle,draw,color=black,fill=black] at (7,-7) {};
	\draw [line width=2pt] (-.5,-7) -- (3,-7);
	\draw [dotted] (3,-7) -- (7,-7);
	\draw [line width=2pt] (7,-7) -- (11,-7);
	\node [circle,draw,color=black,fill=white] at (3,-7) {};
	\node [anchor=west] at (9,-6.65) {$\cv{p(x) \xor q(x)} = [-\infty, 3[ \cup [7, +\infty[$};
	
	%p => q
	\node [circle,draw,color=black,fill=black] at (3,-8) {};
	\draw [line width=2pt] (3,-8) -- (11,-8);
	\draw [dotted] (-.5,-8) -- (3,-8);
	\node [anchor=west] at (9,-7.65) {$\cv{p(x) \then q(x)} = [3, +\infty[$};
	
	%q => p
	\draw [line width=2pt] (-.5,-9) -- (7,-9);
	\draw [dotted] (7,-9) -- (11,-9);
	\node [circle,draw,color=black,fill=white] at (7,-9) {};
	\node [anchor=west] at (9,-8.65) {$\cv{q(x) \then p(x)} = ]-\infty, 7[$};
	
	%p <=> q	
	\draw [dotted] (-.5,-10) -- (3,-10);
	\draw [line width=2pt] (3,-10) -- (7,-10);
	\draw [dotted] (7,-10) -- (11,-10);
	\node [circle,draw,color=black,fill=black] at (3,-10) {};	
	\node [circle,draw,color=black,fill=white] at (7,-10) {};
	\node [anchor=west] at (9,-9.65) {$\cv{p(x) \ssi q(x)} = [3, 7[$};

\end{tikzpicture}
\caption{Classes de vérités de prédicats}
\label{fig:exo-res-cv}
\end{center}
\end{figure}

\end{sol}

\end{detail}
