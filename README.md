# Mathématique contextualisée – Syllabus

Remarque : il vous faut une version récente de
[tikz](https://sourceforge.net/projects/pgf/) pour compiler certaines figures.

## Version courte ou détaillée et exos.

Le syllabus se décline en deux versions, une courte et une détaillée. On peut
compiler explicitement la version souhaitée en compilant le fichier
`syllabus-detail.tex` ou bien `syllabus-short.tex`.

Le fichier `syllabus.tex` peut également être compilé, auquel cas, la version
compilée dépend du contenu de `detail-conf.tex`. La variable booléenne `detail`
doit être mise à true ou false selon que l'on souhaite la version détaillée ou
non.

En plus de ces deux versions, le syllabus d'exercices est disponible via
`syllabus-exos.tex`.

## Compiler avec `make` et `latexmk`

Utilisez les commandes suivantes pour compiler pour
- la version courte : `make syllabus-short.pdf` ou `make short`;
- la version détaillée : `make syllabus-detail.pdf`, `make detail` ou `make` ;
- les exercices : `make syllabus-exos.pdf` ou `make exos`;
- les trois versions : `make all`.

Utilisez `make preview` pour visualiser et re-compiler automatiquement à chaque
changement des sources la sible `syllabus.pdf` (version courte/détaillée selon
`detail-conf.tex`).

## Compiler sans `make` ni `latexmk`

Entrez les commandes suivantes :

1. `pdflatex syllabus.tex`
2. `bibtex syllabus.aux`
3. `makeindex -c syllabus.idx`
4. `pdflatex syllabus.tex`
5. `pdflatex syllabus.tex`

Le document final se nomme `syllabus.pdf`.

Alternativement, compilez `syllabus-exos.tex`, `syllabus-short.tex` ou bien
`syllabus-detail.tex`.
